/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package article_classification;

import static article_classification.Classification.train;
import java.net.MalformedURLException;
import java.net.URISyntaxException;

/**
 *
 * @author herc
 */
public class Article {

    public String url;
    public String body;
    public String category;

    public Article(String url, String title_crawling) throws MalformedURLException, URISyntaxException {
        this.url = StaticURLUtilities.fix(url, url);
    }

    @Override
    public String toString() {
        return "Article{" + "url=" + url + ", body=" + body + ", category=" + category + '}';
    }

    public static void exportModel() throws Exception {
        train();
    }
}
