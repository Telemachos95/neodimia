package article_classification;

import static gui.MainFrame.stopwords;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import meka.classifiers.multilabel.Evaluation;
import meka.classifiers.multilabel.RT;
import meka.core.MLUtils;
import meka.core.Result;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils.DataSource;
import weka.core.stemmers.NullStemmer;
import weka.core.tokenizers.WordTokenizer;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToWordVector;

public class Classification {
    
    public static void train() throws FileNotFoundException, IOException, Exception {
        
        Instances data = DataSource.read(System.getProperty("user.dir") + "/files/classification/level1/meka_training.arff");
        MLUtils.prepareData(data);
        data.setClassIndex(16);
        Instances structure = data.stringFreeStructure();
        String relation = data.relationName();
        weka.core.SerializationHelper.write(System.getProperty("user.dir") + "/files/classification/level1/structure.arff", structure);
        
        int counters[] = new int[17];
        Attribute attr;
        
        for (int i = 0; i < data.size(); i++) {
            for (int j = 0; j < 17; j++) {
                attr = data.attribute(j);
                if (data.get(i).stringValue(attr).contains("1")) {
                    counters[j]++;
                }
            }
        }
        
        System.out.println(Arrays.toString(counters));
        
        StringToWordVector vector = new StringToWordVector();
        vector.setInputFormat(data);
        vector.setDoNotOperateOnPerClassBasis(false);
        vector.setWordsToKeep(10000);
        vector.setStopwords(new File(System.getProperty("user.dir") + "/resources/stopwords.txt"));
        vector.setUseStoplist(true);
        WordTokenizer tokenizer = new WordTokenizer();
        tokenizer.setDelimiters(" 	.,;:'\"()?!");
        vector.setTokenizer(tokenizer);
        NullStemmer stemmer = new NullStemmer();
        vector.setStemmer(stemmer);
        vector.setLowerCaseTokens(true);
        
        data = Filter.useFilter(data, vector);
        
        RT classifier = new RT();
        String[] options = new String[2];
        options[0] = "-W";
        options[1] = "weka.classifiers.functions.LibLINEAR";
        classifier.setOptions(options);
        classifier.buildClassifier(data);
        
        weka.core.SerializationHelper.write(System.getProperty("user.dir") + "/files/classification/level1/meka_training.filter", vector);
        weka.core.SerializationHelper.write(System.getProperty("user.dir") + "/files/classification/level1/meka_training.model", classifier);
        
        Result result = Evaluation.cvModel(classifier, data, 10, "PCut1");
        System.out.println(result);

//        System.out.println(data);
        ArffSaver saver = new ArffSaver();
        data.setRelationName(relation);
        saver.setInstances(data);
        saver.setFile(new File(System.getProperty("user.dir") + "/files/classification/level1/test_classifiers.arff"));
        saver.writeBatch();
    }
    
    public double[] test(Classification c, Article article) {
        double[] dist = null;
        try {
            RT fc = (RT) weka.core.SerializationHelper.read(System.getProperty("user.dir") + "/files/classification/level1/meka_training.model");
            StringToWordVector filter = (StringToWordVector) weka.core.SerializationHelper.read(System.getProperty("user.dir") + "/files/classification/level1/meka_training.filter");
//            article.body = processStringByTF(article.body, 2, article.url, 3);
            Instances isTestSet = c.getDataSet(article.body);
//            System.out.println(isTestSet);
            Instances newTestSet = Filter.useFilter(isTestSet, filter);
            dist = fc.distributionForInstance(newTestSet.instance(0));
            return dist;
        } catch (Exception ex) {
            Logger.getLogger(Classification.class.getName()).log(Level.SEVERE, null, ex);
        }
        return dist;
    }
    
    private Instances getDataSet(String body) throws Exception {
        Instances data = (Instances) weka.core.SerializationHelper.read(System.getProperty("user.dir") + "/files/classification/level1/structure.arff");
        try {
            body = preprocess(body);
            double[] newInst = new double[18];
            for (int i = 0; i < 16; i++) {
                newInst[i] = 0.0;
            }
            newInst[17] = (double) data.attribute(17).addStringValue(body);
            DenseInstance instance = new DenseInstance(1.0, newInst);
            data.add(instance);
            return data;
        } catch (Exception ex) {
            Logger.getLogger(Classification.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }
    
    private String preprocess(String body) {
        String reply = "";
        String term;
        ArrayList<String> tokens = new ArrayList<>();
//        body = body.replaceAll("έ", "ε").replaceAll("ό", "ο").replaceAll("ή", "η").replaceAll("ώ", "ω").replaceAll("ά", "α").replaceAll("ί", "ι").replaceAll("ύ", "υ").replaceAll("ϊ", "ι").replaceAll("ϋ", "υ").replaceAll("ΐ", "ι");
        StringTokenizer st = new StringTokenizer(body, " 	.,;:'\"()?!");
        while (st.hasMoreTokens()) {
            term = st.nextToken().trim();
            if (term.length() == 1) {
                continue;
            }
            if (stopwords.contains(term)) {
                continue;
            }
            if (!term.matches("[0-9]+.*[0-9]+") && !term.matches("[0-9]+.*") && !term.matches(".*[0-9]+")) {
                NullStemmer stemmer = new NullStemmer();
                term = stemmer.stem(term);
                tokens.add(term);
            }
        }
        for (String token : tokens) {
            reply += " " + token.toLowerCase(new java.util.Locale("el"));
        }
        return reply;
    }
    
}
