/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package article_classification;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.DefaultHttpClient;
import org.htmlparser.lexer.Page;

/**
 *
 * @author varlamis
 */
public class StaticURLUtilities {

    public static HashSet<String> stopwords = readStopwords((System.getProperty("user.dir") + "/resources/stopwords.txt"));

    public static String fix(String url, String categoryPage) throws MalformedURLException, URISyntaxException {
        Page home = new Page();
        home.setUrl(categoryPage);
        String newurl = home.getAbsoluteURL(url);
        URL urlt = new URL(newurl);
        URI uri = new URI(urlt.getProtocol(), urlt.getUserInfo(), urlt.getHost(), urlt.getPort(), urlt.getPath(), urlt.getQuery(), urlt.getRef());
        urlt = uri.toURL();
        return urlt.toString();
    }

    private static char toHex(int ch) {
        return (char) (ch < 10 ? '0' + ch : 'A' + ch - 10);
    }

    private static boolean isUnsafe(char ch) {
        if (ch > 128 || ch < 0) {
            return true;
        }
        return " %$&+,/:;=?@<>#%".indexOf(ch) >= 0;
    }

    public static String greekToGreeklish(String greek) {
        String reply = "" + greek;
        reply.replaceAll("α", "a");
        reply.replaceAll("β", "v");
        reply.replaceAll("γ", "g");
        reply.replaceAll("δ", "d");
        reply.replaceAll("ε", "e");
        reply.replaceAll("ζ", "z");
        reply.replaceAll("η", "h");
        reply.replaceAll("θ", "th");
        reply.replaceAll("ι", "i");
        reply.replaceAll("κ", "k");
        reply.replaceAll("λ", "l");
        reply.replaceAll("μ", "m");
        reply.replaceAll("ν", "n");
        reply.replaceAll("ξ", "ks");
        reply.replaceAll("ο", "o");
        reply.replaceAll("π", "p");
        reply.replaceAll("ρ", "r");
        reply.replaceAll("σ", "s");
        reply.replaceAll("τ", "t");
        reply.replaceAll("υ", "y");
        reply.replaceAll("φ", "f");
        reply.replaceAll("χ", "x");
        reply.replaceAll("ψ", "ps");
        reply.replaceAll("ω", "w");

        return reply;

    }

    public static String getHost(String categoryPage) throws MalformedURLException {
        URL u = new URL(categoryPage);
        String domain = u.getHost();
        return domain;
    }

    public static String getPath(String categoryPage) {
        String path = null;
        try {
            URL u = new URL(categoryPage);
            path = u.getPath();
        } catch (java.net.MalformedURLException malex) {
            malex.printStackTrace();
        }
        return path;
    }

    public static String getQuery(String categoryPage) throws MalformedURLException {
        URL u = new URL(categoryPage);
        return u.getQuery();
    }

    public static HashSet<String> tokenize(String input) {
        String mapped = removeAccents(input);
        String[] tokens = mapped.split("\\[|\\]|»|«|\\$|\\(|\\)|\\+|\"|-|\\.|:|,|”|“|\\s+");
        HashSet<String> toklist = new HashSet<String>();
        for (String s : tokens) {
            s = s.trim();
            if (s.length() <= 1) {
                continue;
            }
            if (s.startsWith("<") && s.endsWith(">")) {
                continue;
            }
            if (stopwords.contains(s)) {
                continue;
            }
            toklist.add(s);
        }
        return toklist;
    }

    public static double cosine(HashMap<String, Integer> a, HashMap<String, Integer> b) {
        double sum = 0;
        double norma = 0, normb = 0;
        for (Entry<String, Integer> ben : b.entrySet()) {
            int av = (a.get(ben.getKey()) == null ? 0 : a.get(ben.getKey()));
            sum += av * ben.getValue();
            normb += Math.pow(ben.getValue(), 2);
        }
        for (Entry<String, Integer> aen : a.entrySet()) {
            norma += Math.pow(aen.getValue(), 2);
        }
//        return sum;
        if (norma * normb == 0) {
            return 0;
        } else {
            return sum / (Math.sqrt(norma) * Math.sqrt(normb));
        }
    }

    public static HashMap<String, Integer> tokenizeTF(String input) {
        String mapped = removeAccents(input);
        String[] tokens = mapped.split("»|«|\\$|\\(|\\)|\\+|\"|-|\\.|:|/|,|\\s+");
        HashMap<String, Integer> tokmap = new HashMap<String, Integer>();
        for (String s : tokens) {
            s = s.trim();
            if (s.length() <= 1) {
                continue;
            }
            if (s.startsWith("<") && s.endsWith(">")) {
                continue;
            }
            if (stopwords.contains(s)) {
                continue;
            }
            if (s.matches("\\d+")) {
                continue;
            }
            if (tokmap.containsKey(s)) {
                tokmap.put(s, tokmap.get(s).intValue() + 1);
            } else {
                tokmap.put(s, 1);
            }
        }
        return tokmap;
    }
    /*NEW SEPTEMBER*/

    public static ArrayList<StringTF> orderByTFAndFilter(HashMap<String, Integer> terms, int minOccurFilter) {
        ArrayList<StringTF> allterms = new ArrayList<>();
        for (String k : terms.keySet()) {
            if (terms.get(k) >= minOccurFilter) {
                allterms.add(new StringTF(k, terms.get(k)));
            }
        }
        Collections.sort(allterms);
        return allterms;

    }

    public static String processStringByTF(String originalString, int minOccurences, String categoryOrUrl, int multiplyUrlTermsBy) {

        HashMap<String, Integer> keys = StaticURLUtilities.tokenizeTF(originalString);
        ArrayList<StringTF> mft = StaticURLUtilities.orderByTFAndFilter(keys, minOccurences);
        HashMap<String, Integer> urlkeys = StaticURLUtilities.tokenizeTF(categoryOrUrl);
        for (Entry<String, Integer> a : urlkeys.entrySet()) {
            a.setValue(a.getValue() * multiplyUrlTermsBy);
        }
        ArrayList<StringTF> mfturl = StaticURLUtilities.orderByTFAndFilter(urlkeys, 1);

        String processedBody = "";
        for (StringTF x : mft) {
            processedBody += " " + x.toTextString();
        }
        for (StringTF x : mfturl) {
            processedBody += " " + x.toTextString();
        }
        return processedBody;
    }

    public static ArrayList<String> split(String input) {
        String mapped = removeAccents(input);
        String[] tokens = mapped.split("\\n|\\t|»|«|\\$|\\(|\\)|\\+|\"|-|\\.|:|/|,|\\s+");
        ArrayList<String> toklist = new ArrayList<String>();
        for (String s : tokens) {
            s = s.trim();
            if ((s.startsWith("<") && s.endsWith(">")) || (s.length() == 0)) {
                continue;
            }
            toklist.add(s);
        }
        return toklist;
    }

    public static String removeAccents(String input) {
        String out = "";
        try {
            out = input.toLowerCase().replaceAll("ά", "α").replaceAll("έ", "ε").replaceAll("ί", "ι").replaceAll("ό", "ο").replaceAll("ύ", "υ").replaceAll("ή", "η").replaceAll("ώ", "ω");
        } catch (NullPointerException nulex) {
            System.err.println("RemoveAccents failed for:" + input);
        }
        return out;
    }

    public static HashSet<String> readStopwords(String filename) {
        HashSet<String> stopwords = new HashSet<String>();
        String line = "";
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            line = br.readLine();
            while (line != null) {
                stopwords.add(line);
                line = br.readLine();
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return stopwords;
    }

    public static Integer head(String uri) throws IOException {
        HttpHead headMethod = new HttpHead(uri);
        DefaultHttpClient client = new DefaultHttpClient();
        try {
            HttpResponse response = client.execute(headMethod);
            Header firstHeader = response.getFirstHeader("Content-Length");
            return (firstHeader == null) ? null : Integer.parseInt(firstHeader.getValue());
        } finally {
            headMethod.releaseConnection();
            client.getConnectionManager().shutdown();
        }
    }

    public static boolean domainsMatch(String domain, String target) throws MalformedURLException {
        boolean skip = true;
        String domhost = getHost(domain);
        String cathost = getHost(target);
        if (!domhost.equalsIgnoreCase(cathost)) {
            if (domhost.startsWith("www.")) {
                domhost = domhost.substring(4);
                if (cathost.endsWith(domhost)) {
                    skip = false;
                }
            }
        } else {
            skip = false;
        }
        return skip;
    }
}
