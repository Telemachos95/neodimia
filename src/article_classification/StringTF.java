/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package article_classification;

import java.util.Comparator;

/**
 *
 * @author varlamis
 */
public class StringTF implements Comparable<StringTF> {

    String word;
    int occurences;

    public StringTF(String word, int occurences) {
        this.word = word;
        this.occurences = occurences;
    }

    public int compareTo(StringTF b) {
        if (this.occurences < b.occurences) {
            return 1;
        } else if (this.occurences > b.occurences) {
            return -1;
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return word + ":" + occurences;
    }
    
    public String toTextString(){
        String x=word;
        for (int i=1;i<occurences;i++){
            x+=" "+word;
        }
        return x;
    }
}
