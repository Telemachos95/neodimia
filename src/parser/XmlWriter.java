/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import static gui.MainFrame.trainingData;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Tilemachos
 */
public class XmlWriter {

    File xml;

    public XmlWriter(File xml) {
        this.xml = xml;
    }

    public XmlWriter() {

    }

    public void updateXml(String link, ArrayList<String> categories) {
        try {
            File file = xml.getAbsoluteFile();
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            NodeList nList = doc.getElementsByTagName("Article");
            if (nList.getLength() > 0) {
                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);

                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element eElement = (Element) nNode;
                        String link1 = eElement.getElementsByTagName("link").item(0).getTextContent();
                        if (link.equals(link1)) {
                            Element subject = doc.createElement("subject");
                            eElement.appendChild(subject);
                            for (int i = 0; i < categories.size(); i++) {
                                int tmp = i + 1;
                                String temp1 = Integer.toString(tmp);
                                String temp2 = "key" + temp1;
                                subject.setAttribute(temp2, categories.get(i));
                            }
                            TransformerFactory transformerFactory
                                    = TransformerFactory.newInstance();
                            Transformer transformer
                                    = transformerFactory.newTransformer();
                            DOMSource source = new DOMSource(doc);
                            StreamResult result
                                    = new StreamResult((xml.getAbsoluteFile()));
                            transformer.transform(source, result);
                        }
                    }
                }
            }

        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(XmlWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(XmlWriter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (TransformerException ex) {
            Logger.getLogger(XmlWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
