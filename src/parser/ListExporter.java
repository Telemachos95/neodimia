/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import static gui.MainFrame.Words;
import static gui.MainFrame.alternatives;
import static gui.MainFrame.neologismoi;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;
import javax.swing.JTextArea;

/**
 *
 * @author Tilemachos
 */
public class ListExporter {

    public static void exportList(JTextArea textArea) throws FileNotFoundException, UnsupportedEncodingException {

        File theDir = new File("Κατάλογοι_αποθήκευσης");

// if the directory does not exist, create it
        if (!theDir.exists()) {
            System.out.println("creating directory: " + "Κατάλογοι_αποθήκευσης");
            boolean result = false;

            try {
                theDir.mkdir();
                result = true;
            } catch (SecurityException se) {
                //handle it
            }
            if (result) {
                System.out.println("DIR Κατάλογοι_αποθήκευσης created");
            }
        }

        Date date = new Date();
        String editedDate = date.toString().replaceAll(" ", "_").replaceAll(":", "-");
        PrintWriter pw = new PrintWriter(new File(System.getProperty("user.dir") + "/Κατάλογοι_αποθήκευσης/Κατάλογος_αποθήκευσης_" + editedDate + ".csv"), "ISO-8859-7");
        StringBuilder sb = new StringBuilder();
        sb.append("Αρχεία Xml:");
        sb.append(' ');

        String text = textArea.getText();
        String[] arr = text.split("\n");
        for (String temp : arr) {
            sb.append(temp);
            sb.append(',');
        }

        sb.append('\n');

        sb.append("Αρίθμηση");
        sb.append(',');
        sb.append("Υποψήφιος Νεολογισμός");
        sb.append(',');
        sb.append("Εμφανίσεις");
        sb.append(',');
        sb.append("Αριθμός Άρθρων");
        sb.append('\n');
        int emfaniseis;
        int articleNumber;
        Set<String> set = new LinkedHashSet<>();

        for (int i = 0; i < alternatives.size(); i++) {
            String[] splited = alternatives.get(i).getSimilarWords().split(" ");
            for (String s : splited) {
                emfaniseis = 0;
                articleNumber = 0;
                set.clear();
                for (Word test2 : Words) {
                    String lima = test2.nonAccentedWord.toUpperCase(new java.util.Locale("el"));
                    if (s.equals(lima)) {
                        if (set.add(test2.title)) {
                            articleNumber++;
                        }
                        emfaniseis++;
                    }
                }
                sb.append(i + 1);
                sb.append(',');
                sb.append(' ');
                sb.append(s);
                sb.append(',');
                sb.append(emfaniseis);
                sb.append(',');
                sb.append(articleNumber);
                sb.append('\n');
            }

        }

        pw.write(sb.toString());
        pw.close();
        System.out.println("File Saved!");
    }

    public static void exportListEnglish(JTextArea textArea) throws FileNotFoundException, UnsupportedEncodingException {
        File theDir = new File("Κατάλογοι_αποθήκευσης_ξενογλ");

// if the directory does not exist, create it
        if (!theDir.exists()) {
            System.out.println("creating directory: " + "Κατάλογοι_αποθήκευσης_ξενογλ");
            boolean result = false;

            try {
                theDir.mkdir();
                result = true;
            } catch (SecurityException se) {
                //handle it
            }
            if (result) {
                System.out.println("DIR Κατάλογοι_αποθήκευσης_ξενογλ created");
            }
        }

        Date date = new Date();
        String editedDate = date.toString().replaceAll(" ", "_").replaceAll(":", "-");
        PrintWriter pw = new PrintWriter(new File(System.getProperty("user.dir") + "./Κατάλογοι_αποθήκευσης_ξενογλ/Κατάλογος_αποθήκευσης_ξενογλ_" + editedDate + ".csv"), "ISO-8859-7");
        StringBuilder sb = new StringBuilder();
        sb.append("Αρχεία Xml:");
        sb.append(' ');

        String text = textArea.getText();
        String[] arr = text.split("\n");
        for (String temp : arr) {
            sb.append(temp);
            sb.append(',');
        }

        sb.append('\n');

        sb.append("Αρίθμηση");
        sb.append(',');
        sb.append("Υποψήφιος Νεολογισμός");
        sb.append(',');
        sb.append("Εμφανίσεις");
        sb.append(',');
        sb.append("Αριθμός Άρθρων");
        sb.append('\n');
        int emfaniseis;
        int articleNumber;
        Set<String> set = new LinkedHashSet<>();

        for (int i = 0; i < alternatives.size(); i++) {
            String[] splited = alternatives.get(i).getSimilarWords().split(" ");
            for (String s : splited) {
                emfaniseis = 0;
                articleNumber = 0;
                set.clear();
                for (Word test2 : Words) {
                    String lima = test2.nonAccentedWord.toUpperCase(new java.util.Locale("el"));
                    if (s.equals(lima)) {
                        if (set.add(test2.title)) {
                            articleNumber++;
                        }
                        emfaniseis++;
                    }
                }
                sb.append(i + 1);
                sb.append(',');
                sb.append(' ');
                sb.append(s);
                sb.append(',');
                sb.append(emfaniseis);
                sb.append(',');
                sb.append(articleNumber);
                sb.append('\n');
            }

        }

        pw.write(sb.toString());
        pw.close();
        System.out.println("File Saved!");
    }
}
