/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import java.util.ArrayList;

/**
 *
 * @author Tilemachos
 */
public class TrainingData {

    public String keimeno;
    public ArrayList<String> categories;

    public TrainingData(String keimeno, ArrayList<String> categories) {
        this.keimeno = keimeno;
        this.categories = categories;
    }

    public String getKeimeno() {
        return keimeno;
    }

    public void setKeimeno(String keimeno) {
        this.keimeno = keimeno;
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<String> categories) {
        this.categories = categories;
    }

}
