/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

public class FileListAccessory extends JComponent implements PropertyChangeListener {

    private File file = null;
    private DefaultListModel model;
    private JList list;
    private JButton removeItem;
    private JButton addItem;
    private JButton addMultipleItems;
    private ArrayList<File> fileList = new ArrayList();
    private int counter = 0;

    public FileListAccessory(JFileChooser chooser) {
        chooser.addPropertyChangeListener(this);
        model = new DefaultListModel();
        list = new JList(model);
        JScrollPane pane = new JScrollPane(list);
        pane.setPreferredSize(new Dimension(200, 250));

        removeItem = createRemoveItemButton();
        addItem = createAddItemButton();
        addMultipleItems = createAddMultipleItemsButton();
        addMultipleItems.setEnabled(false);
        setBorder(new EmptyBorder(10, 10, 10, 10));
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        add(addItem);
        add(addMultipleItems);
        add(pane);
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        add(removeItem);
    }

    public DefaultListModel getModel() {
        return model;
    }

    private void addFileToList() {
        if (file != null) {
            model.addElement(file);
        }
    }

    private void removeFileFromList() {
        if (list.getSelectedIndex() != -1) {
            model.remove(list.getSelectedIndex());
        }
    }

    private JButton createRemoveItemButton() {
        JButton button = new JButton("Αφαίρεση");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                removeFileFromList();
            }
        });
        return button;
    }

    private JButton createAddItemButton() {
        JButton button = new JButton("Προσθήκη");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addFileToList();
            }
        });
        return button;
    }

    private JButton createAddMultipleItemsButton() {
        JButton button = new JButton("Προσθήκη πολλαπλών αρχείων");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (fileList.size() > 1) {
                    for (File file : fileList) {
                        if (file != null && !file.isDirectory()) {
                            model.addElement(file);
                        }
                    }
                    fileList.clear();
                    addMultipleItems.setEnabled(false);
                    counter = 0;
                }
            }
        });
        return button;
    }

    @Override
    public void propertyChange(PropertyChangeEvent e) {
        boolean update = false;
        String prop = e.getPropertyName();

        //If the directory changed, don't do anything
        if (JFileChooser.DIRECTORY_CHANGED_PROPERTY.equals(prop)) {
            file = null;
            update = true;
            //If a file became selected, find out which one.
        } else if (JFileChooser.SELECTED_FILE_CHANGED_PROPERTY.equals(prop)) {
            file = (File) e.getNewValue();
            if (file != null && !file.isDirectory()) {
                fileList.add(file);
                counter++;
                if (counter > 1) {
                    addMultipleItems.setEnabled(true);
                }
            }
            update = true;
        }
    }
}
