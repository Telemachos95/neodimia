/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 *
 * @author Tilemachos
 */
public class GetPropertyValues {

    String result = "";
    InputStream inputStream;
    public static String address;
    public static String serverPort;

    public static String getServerPort() {
        return serverPort;
    }

    public static void setServerPort(String serverPort) {
        GetPropertyValues.serverPort = serverPort;
    }
    public static String port;
    public static String username;
    public static String password;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public static String getAddress() {
        return address;
    }

    public static void setAddress(String address) {
        GetPropertyValues.address = address;
    }

    public static String getPort() {
        return port;
    }

    public static void setPort(String port) {
        GetPropertyValues.port = port;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        GetPropertyValues.username = username;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        GetPropertyValues.password = password;
    }

    public String getPropValues() throws IOException {

        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }

            // get the property value and print it out
            address = prop.getProperty("address");
            serverPort = prop.getProperty("serverPort");
            port = prop.getProperty("port");
            username = prop.getProperty("username");
            password = prop.getProperty("password");

            result = "Network Configuration = " + address + ", " + serverPort + ", " + port + ", " + username + ", " + password;
            System.out.println(result);
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            inputStream.close();
        }
        return result;
    }

    public void setPropValues() throws IOException {
        Properties prop = new Properties();
        OutputStream output = null;

        try {

            output = new FileOutputStream(new File(".").getAbsolutePath() + "/resources/config.properties");

            // set the properties value
            prop.setProperty("address", address);
            prop.setProperty("serverPort", serverPort);
            prop.setProperty("port", port);
            prop.setProperty("username", username);
            prop.setProperty("password", password);

            // save properties to project root folder
            prop.store(output, null);
            result = "Network Configuration = " + address + ", " + serverPort + ", " + port + ", " + username + ", " + password;
            System.out.println(result + "\nProperties file saved.");
        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
