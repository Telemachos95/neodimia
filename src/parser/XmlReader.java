/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this titlelate file, choose Tools | Templates
 * and open the titlelate in the editor.
 */
package parser;

import article_classification.Article;
import article_classification.Classification;
import static gui.MainFrame.Keimena;
import static gui.MainFrame.Strings;
import static gui.MainFrame.Titles;
import static gui.MainFrame.Words;
import static gui.MainFrame.Xmls;
import static gui.MainFrame.classificationCategories;
import static gui.MainFrame.modelList;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.DefaultListModel;
import org.w3c.dom.NamedNodeMap;
import weka.core.Utils;

/**
 *
 * @author Tilemachos
 */
public class XmlReader {

    DefaultListModel model;       //Used for choosing the file. 
    ArrayList<File> files = new ArrayList<>();
    File xml;
    String xmlName = "";
    StringBuffer buffer;

    @SuppressWarnings("empty-statement")
    public XmlReader() {
        this.model = modelList.get(0);                  //Constructor and initialization of variables.
        for (int i = 0; i < model.getSize(); i++) {
            xml = (File) model.getElementAt(i);
            if (xml.isFile()) {
                files.add(xml);
            } else {
                File[] listOfFiles = xml.listFiles((File dir, String name) -> name.toLowerCase().endsWith(".xml"));;
                for (File file : listOfFiles) {
                    if (file.isFile()) {
                        files.add(file);
                    }
                }
            }

        }
    }

    public void parseXml() {

        for (File file : files) {
            xml = file.getAbsoluteFile();

            try {

                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(xml);
                xmlName = xml.getName();

                doc.getDocumentElement().normalize();

                NodeList nList = doc.getElementsByTagName("Article");

                if (nList.getLength() > 0) {
                    System.out.println(xml.getName() + " OLD");
                    for (int temp = 0; temp < nList.getLength(); temp++) {

                        Node nNode = nList.item(temp);

                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                            buffer = new StringBuffer();
                            Element eElement = (Element) nNode;
                            String title = eElement.getElementsByTagName("title").item(0).getTextContent();
                            String keimeno = eElement.getElementsByTagName("content").item(0).getTextContent();
                            String link = eElement.getElementsByTagName("link").item(0).getTextContent();
                            String pubdate = eElement.getElementsByTagName("pubdate").item(0).getTextContent();
                            boolean classified = false;
                            int subjects = eElement.getElementsByTagName("subject").getLength();
                            if (subjects > 0) {
                                classified = true;
                            }

                            String article = title + (".") + keimeno;

                            if (article.contains("&quot;")) {
                                article = article.replaceAll("&quot;", "\"");
                                keimeno = keimeno.replaceAll("&quot;", "\"");
                                title = title.replaceAll("&quot;", "\"");
                            }
                            if (article.contains("&apos;")) {
                                article = article.replaceAll("&apos;", "'");
                                keimeno = keimeno.replaceAll("&apos;", "'");
                                title = title.replaceAll("&apos;", "'");
                            }

                            if (article.contains("&lt;")) {
                                article = article.replaceAll("&lt;", "<");
                                keimeno = keimeno.replaceAll("&lt;", "<");
                                title = title.replaceAll("&lt;", "<");
                            }

                            if (article.contains("&gt;")) {
                                article = article.replaceAll("&gt;", ">");
                                keimeno = keimeno.replaceAll("&gt;", ">");
                                title = title.replaceAll("&gt;", ">");
                            }

                            if (article.contains("&amp;")) {
                                article = article.replaceAll("&amp;", "&");
                                keimeno = keimeno.replaceAll("&amp;", "&");
                                title = title.replaceAll("&amp;", "&");
                            }

                            title = title.replaceAll("•", "").replaceAll("\\s{2,}", " ").replaceAll("(\\d+)(:)(\\d+)(,+)", "").replaceAll("(\\d+)(:)(\\d+)(.)(,+)", "").replaceAll("(\\/)(.{10,})(\\/)", "").replaceAll(" \\. ", " ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ");
                            keimeno = keimeno.replaceAll("•", "").replaceAll("\\s{2,}", " ").replaceAll("(\\d+)(:)(\\d+)(,+)", "").replaceAll("(\\d+)(:)(\\d+)(.)(,+)", "").replaceAll("(\\/)(.{10,})(\\/)", "").replaceAll(" \\. ", " ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ");
                            String rawKeimeno = keimeno;
                            Article arthro = new Article(link, title);
                            arthro.body = keimeno;
                            arthro.url = title;
                            arthro.category = "";

                            Classification c = new Classification();
                            double[] predictions = c.test(c, arthro);
                            if (predictions != null) {
                                System.out.println(Utils.arrayToString(predictions));
                            }
                            if (predictions != null) {
                                int flag = 0;
                                for (int j = 0; j < predictions.length; j++) {
                                    double percentage = Math.round(predictions[j] * 100);
                                    if (percentage > 0.5) {
                                        if (flag == 1) {
                                            arthro.category = arthro.category + ",";
                                        }
                                        arthro.category = arthro.category + classificationCategories.get(j);
                                        System.out.print(classificationCategories.get(j) + " " + percentage + ",");
                                        flag = 1;
                                    }
                                    if (j == predictions.length) {
                                        System.out.println();
                                    }
                                }
                                if (flag == 0) {
                                    System.out.println("--");
                                }
                            }
                            System.out.println();
                            arthro.category = arthro.category + "\t\tΕπιτυχής Κατηγοριοποίηση; (ΝΑΙ ΟΧΙ) ";
                            String subject = "";
                            if (classified == true) {
                                NamedNodeMap subs = eElement.getElementsByTagName("subject").item(0).getAttributes();
                                for (int i = 0; i < subs.getLength(); i++) {
                                    subject = subject + " " + subs.item(i).getTextContent();
                                }
                                arthro.category = subject;
                            }

                            keimeno = keimeno + "\n\nΜεταδεδομένα:\n\n" + "Σύνδεσμος στο άρθρο: \n" + link + "\n\nΗμερομ. δημοσίευσης: " + pubdate + "\n\n" + "Θεματική: " + arthro.category + "\n\n";

                            article = article.replaceAll("•", "").replaceAll("\\s{2,}", " ").replaceAll("(\\d+)(:)(\\d+)(,+)", "").replaceAll("(\\d+)(:)(\\d+)(.)(,+)", "").replaceAll("(\\/)(.{10,})(\\/)", "").replaceAll(" \\. ", " ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ");
                            String plainText = article.replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("΄", " ");
                            buffer.append(plainText);
                            Strings.add(buffer);
                            Word le_word = new Word();
                            le_word.keimeno = keimeno;
                            le_word.rawKeimeno = rawKeimeno;
                            le_word.rawXml = xml.getAbsolutePath();
                            le_word.link = link;
                            Keimena.add(le_word);
                            Xmls.add(xmlName);
                            Titles.add(title);
                            String editedArticle = article.replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("[^\\p{InGreek} -]", " ").replaceAll("΄", " ");

                            String[] articleArray = editedArticle.split("\\s+");

                            for (String tmp : articleArray) {
                                if (tmp != null && tmp.length() > 2) {
                                    String peza = tmp.toLowerCase(new java.util.Locale("el"));
                                    if (!peza.contains("-")) {
                                        String nonAccented = stripAccents(peza);
                                        Word word = new Word(peza, nonAccented, tmp, xmlName, title, keimeno, xml.getAbsolutePath(), link, rawKeimeno);
                                        word.setClassified(classified);
                                        Words.add(word);
                                    } else if (peza.startsWith("-")) {
                                        String startHyphen = peza.replaceAll("-", "");
                                        String nonAccented = stripAccents(startHyphen);
                                        Word word = new Word(peza, nonAccented, tmp, xmlName, title, keimeno, xml.getAbsolutePath(), link, rawKeimeno);
                                        word.setClassified(classified);
                                        Words.add(word);
                                    } else if (peza.endsWith("-")) {
                                        String endHyphen = peza.replaceAll("-", "");
                                        String nonAccented = stripAccents(endHyphen);
                                        Word word = new Word(peza, nonAccented, tmp, xmlName, title, keimeno, xml.getAbsolutePath(), link, rawKeimeno);
                                        word.setClassified(classified);
                                        Words.add(word);
                                    } else {
                                        String nonAccented = stripAccents(peza);
                                        Word word = new Word(peza, nonAccented, tmp, xmlName, title, keimeno, xml.getAbsolutePath(), link, rawKeimeno);
                                        word.setClassified(classified);
                                        Words.add(word);
                                    }
                                }
                            }

                        }
                    }
                } else {
                    parseTei();
                }
            } catch (ParserConfigurationException | IOException e) {
            } catch (SAXException ex) {
                Logger.getLogger(XmlReader.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(XmlReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void parseTei() {

        try {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xml);
            xmlName = xml.getName();
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("TEI");
            if (nList.getLength() == 0) {
                System.out.println("Empty File");
                return;
            }
            System.out.println(xml.getName() + " TEI");
            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    buffer = new StringBuffer();
                    Element eElement = (Element) nNode;
                    String title = "";
                    String keimeno = "";
                    String article = "";
                    String type = "";
                    String degree = "";
                    String edition = "";
                    String publisher = "";
                    String author = "";
                    String date = "";
                    String time = "";
                    String ref = "";
                    String genre = "";
                    String subject = "";
                    String textType = "";

                    if (!eElement.getElementsByTagName("author").item(0).getAttributes().item(0).getNodeValue().isEmpty()) {
                        publisher = eElement.getElementsByTagName("author").item(0).getAttributes().item(0).getTextContent();
                    }

                    if (!eElement.getElementsByTagName("publisher").item(0).getAttributes().item(0).getNodeValue().isEmpty()) {
                        publisher = eElement.getElementsByTagName("publisher").item(0).getAttributes().item(0).getTextContent();
                    }

                    if (!eElement.getElementsByTagName("date").item(0).getAttributes().item(0).getNodeValue().isEmpty()) {
                        date = "Ημεροχρονολογία " + eElement.getElementsByTagName("date").item(0).getAttributes().item(0).getTextContent();
                        date = " Ημέρα " + eElement.getElementsByTagName("date").item(0).getAttributes().item(1).getTextContent();
                        date = " Τρίμηνο " + eElement.getElementsByTagName("date").item(0).getAttributes().item(2).getTextContent();
                    }

                    if (!eElement.getElementsByTagName("time").item(0).getAttributes().item(0).getNodeValue().isEmpty()) {
                        time = eElement.getElementsByTagName("time").item(0).getAttributes().item(0).getTextContent();
                    }

                    if (!eElement.getElementsByTagName("ref").item(0).getAttributes().item(0).getNodeValue().isEmpty()) {
                        ref = eElement.getElementsByTagName("ref").item(0).getAttributes().item(0).getTextContent();
                    }
                    
                    if (!eElement.getElementsByTagName("genre").item(0).getAttributes().item(0).getNodeValue().isEmpty()) {
                        genre = eElement.getElementsByTagName("genre").item(0).getAttributes().item(0).getTextContent();
                    }

                    if (!eElement.getElementsByTagName("ndc:subject").item(0).getAttributes().item(0).getNodeValue().isEmpty()) {
                        subject = eElement.getElementsByTagName("ndc:subject").item(0).getAttributes().item(0).getTextContent();
                    }

                    if (!eElement.getElementsByTagName("edition").item(0).getAttributes().item(0).getNodeValue().isEmpty()) {
                        edition = eElement.getElementsByTagName("edition").item(0).getAttributes().item(0).getTextContent();
                    }

                    if (eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeName().equals("type") && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeValue().isEmpty() && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(1).getNodeValue().isEmpty()) {
                        type = eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getTextContent();
                        degree = eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(1).getTextContent();
                    }

                    if (eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeName().equals("degree") && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeValue().isEmpty() && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(1).getNodeValue().isEmpty()) {
                        type = eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(1).getTextContent();
                        degree = eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getTextContent();
                    }

                    if (!eElement.getElementsByTagName("ndc:textType").item(0).getAttributes().item(0).getNodeValue().isEmpty()) {
                        textType = eElement.getElementsByTagName("ndc:textType").item(0).getAttributes().item(0).getTextContent();
                    }

                    int debug = 4;

                    if (eElement.getElementsByTagName("head").item(0).getAttributes().item(0).getNodeValue().equals("surTitle")) {
                        title = eElement.getElementsByTagName("head").item(1).getTextContent();
                        if (!eElement.getElementsByTagName("head").item(0).getTextContent().isEmpty()) {
                            keimeno = "\nΥπέρτιτλος: " + eElement.getElementsByTagName("head").item(0).getTextContent() + "\n\n";
                        }
                    } else {
                        title = eElement.getElementsByTagName("head").item(0).getTextContent();
                        article = eElement.getElementsByTagName("surTitle").item(0).getTextContent();
                        if (!eElement.getElementsByTagName("surTitle").item(0).getTextContent().isEmpty()) {
                            keimeno = "\nΥπέρτιτλος: " + eElement.getElementsByTagName("surTitle").item(0).getTextContent() + "\n\n";
                        }
                        debug = 3;
                    }

                    if (eElement.getElementsByTagName("head").item(2).getAttributes().item(0).getNodeValue().equals("subTitle") && !eElement.getElementsByTagName("head").item(2).getTextContent().isEmpty()) {
                        keimeno = keimeno + "\nΥπότιτλος: " + eElement.getElementsByTagName("head").item(2).getTextContent() + "\n\n";
                    }

                    for (int i = 0; i < eElement.getElementsByTagName("head").getLength(); i++) {
                        if (eElement.getElementsByTagName("head").item(i).getAttributes().item(0).getNodeValue().equals("midTitle") && !eElement.getElementsByTagName("head").item(i).getTextContent().isEmpty()) {
                            keimeno = keimeno + "\nΜεσότιτλος: " + eElement.getElementsByTagName("head").item(i).getTextContent() + "\n\n";
                        }
                    }

                    if (eElement.getElementsByTagName("head").item(debug).getAttributes().item(0).getNodeValue().equals("lead") && !eElement.getElementsByTagName("head").item(debug).getTextContent().isEmpty()) {
                        keimeno = keimeno + "\nΠαράγραφος Lead: " + eElement.getElementsByTagName("head").item(debug).getTextContent() + "\n\n";
                    }

                    for (int i = 0; i < eElement.getElementsByTagName("head").getLength(); i++) {
                        article = article + eElement.getElementsByTagName("head").item(i).getTextContent() + " ";
                    }

                    if (!eElement.getElementsByTagName("caption").item(0).getTextContent().isEmpty()) {
                        article = article + eElement.getElementsByTagName("caption").item(0).getTextContent() + " ";
                        keimeno = keimeno + "\nΛεζάντα: " + eElement.getElementsByTagName("caption").item(0).getTextContent() + "\n\n";
                    }

                    for (int y = 0; y < eElement.getElementsByTagName("p").getLength(); y++) {
                        article = article + eElement.getElementsByTagName("p").item(y).getTextContent() + " ";
                        keimeno = keimeno + eElement.getElementsByTagName("p").item(y).getTextContent() + " ";
                    }

                    if (article.contains("&quot;")) {
                        article = article.replaceAll("&quot;", "\"");
                        keimeno = keimeno.replaceAll("&quot;", "\"");
                        title = title.replaceAll("&quot;", "\"");
                    }
                    if (article.contains("&apos;")) {
                        article = article.replaceAll("&apos;", "'");
                        keimeno = keimeno.replaceAll("&apos;", "'");
                        title = title.replaceAll("&apos;", "'");
                    }

                    if (article.contains("&lt;")) {
                        article = article.replaceAll("&lt;", "<");
                        keimeno = keimeno.replaceAll("&lt;", "<");
                        title = title.replaceAll("&lt;", "<");
                    }

                    if (article.contains("&gt;")) {
                        article = article.replaceAll("&gt;", ">");
                        keimeno = keimeno.replaceAll("&gt;", ">");
                        title = title.replaceAll("&gt;", ">");
                    }

                    if (article.contains("&amp;")) {
                        article = article.replaceAll("&amp;", "&");
                        keimeno = keimeno.replaceAll("&amp;", "&");
                        title = title.replaceAll("&amp;", "&");
                    }

                    title = title.replaceAll("•", "").replaceAll("\\s{2,}", " ").replaceAll("(\\d+)(:)(\\d+)(,+)", "").replaceAll("(\\d+)(:)(\\d+)(.)(,+)", "").replaceAll("(\\/)(.{10,})(\\/)", "").replaceAll(" \\. ", " ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ");
                    keimeno = keimeno.replaceAll("•", "").replaceAll("(\\d+)(:)(\\d+)(,+)", "").replaceAll("(\\d+)(:)(\\d+)(.)(,+)", "").replaceAll("(\\/)(.{10,})(\\/)", "").replaceAll(" \\. ", " ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ");
                    title = title.replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("΄", " ");
                    keimeno = keimeno.replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("΄", " ");
                    String rawKeimeno = keimeno;
                    Article arthro = new Article(ref, title);
                    arthro.body = keimeno;
                    arthro.category = "";

                    Classification c = new Classification();
                    double[] predictions = c.test(c, arthro);
                    if (predictions != null) {
                        System.out.println(Utils.arrayToString(predictions));
                    }
                    if (predictions != null) {
                        int flag = 0;
                        for (int j = 0; j < predictions.length; j++) {
                            double percentage = Math.round(predictions[j] * 100);
                            if (percentage > 0.5) {
                                if (flag == 1) {
                                    arthro.category = arthro.category + ",";
                                }
                                arthro.category = arthro.category + classificationCategories.get(j);
                                System.out.print(classificationCategories.get(j) + " " + percentage + ",");
                                flag = 1;
                            }
                            if (j == predictions.length) {
                                System.out.println();
                            }
                        }
                        if (flag == 0) {
                            System.out.println("--");
                        }
                    }
                    System.out.println();

                    arthro.category = arthro.category + "\t\tΕπιτυχής Κατηγοριοποίηση; (ΝΑΙ ΟΧΙ) ";

                    keimeno = keimeno + "\n\nΜεταδεδομένα:\n\nΕφημερίδα: " + publisher + "\n\nΗμερομ. δημοσίευσης: " + date + "\n\nΣύνδεσμος στο άρθρο:  \n" + ref + "\n\nΜορφή έκδοσης: " + edition + "\n\nΘεματική: " + arthro.category + "\n\n";

                    if (eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeName().equals("type") && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeValue().isEmpty() && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(1).getNodeValue().isEmpty() && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeValue().equals(" ") && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(1).getNodeValue().equals(" ")) {
                        keimeno = keimeno + "Τρόπος λόγου: " + type + "  " + degree;
                    }

                    if (eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeName().equals("degree") && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeValue().isEmpty() && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(1).getNodeValue().isEmpty() && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeValue().equals(" ") && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(1).getNodeValue().equals(" ")) {
                        keimeno = keimeno + "Τρόπος λόγου: " + type + "  " + degree;
                    }

                    if (!eElement.getElementsByTagName("ndc:textType").item(0).getAttributes().item(0).getNodeValue().isEmpty() && !eElement.getElementsByTagName("ndc:textType").item(0).getAttributes().item(0).getNodeValue().equals(" ")) {
                        keimeno = keimeno + "\n\nΚειμενικός τύπος: " + textType + "\n\n";
                    }

                    article = article.replaceAll("•", "").replaceAll("\\s{2,}", " ").replaceAll("(\\d+)(:)(\\d+)(,+)", "").replaceAll("(\\d+)(:)(\\d+)(.)(,+)", "").replaceAll("(\\/)(.{10,})(\\/)", "").replaceAll(" \\. ", " ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ");

                    String plainText = article.replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("΄", " ");
                    buffer.append(plainText);
                    Strings.add(buffer);
                    Word le_word = new Word();
                    le_word.keimeno = keimeno;
                    le_word.rawKeimeno = rawKeimeno;
                    le_word.rawXml = xml.getAbsolutePath();
                    le_word.link = ref;
                    Keimena.add(le_word);
                    Xmls.add(xmlName);
                    Titles.add(title);
                    String editedArticle = article.replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("[^\\p{InGreek} -]", " ").replaceAll("΄", " ");

                    String[] articleArray = editedArticle.split("\\s+");

                    for (String tmp : articleArray) {
                        if (tmp != null && tmp.length() > 2) {
                            String peza = tmp.toLowerCase(new java.util.Locale("el"));
                            if (!peza.contains("-")) {
                                String nonAccented = stripAccents(peza);
                                Word word = new Word(peza, nonAccented, tmp, xmlName, title, keimeno, xml.getAbsolutePath(), ref, rawKeimeno);
                                Words.add(word);
                            } else if (peza.startsWith("-")) {
                                String startHyphen = peza.replaceAll("-", "");
                                String nonAccented = stripAccents(startHyphen);
                                Word word = new Word(peza, nonAccented, tmp, xmlName, title, keimeno, xml.getAbsolutePath(), ref, rawKeimeno);
                                Words.add(word);
                            } else if (peza.endsWith("-")) {
                                String endHyphen = peza.replaceAll("-", "");
                                String nonAccented = stripAccents(endHyphen);
                                Word word = new Word(peza, nonAccented, tmp, xmlName, title, keimeno, xml.getAbsolutePath(), ref, rawKeimeno);
                                Words.add(word);
                            } else {
                                String nonAccented = stripAccents(peza);
                                Word word = new Word(peza, nonAccented, tmp, xmlName, title, keimeno, xml.getAbsolutePath(), ref, rawKeimeno);
                                Words.add(word);
                            }
                        }
                    }
                }
            }
        } catch (ParserConfigurationException | IOException e) {
        } catch (SAXException ex) {
            Logger.getLogger(XmlReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(XmlReader.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void parseXmlEnglish() {
        for (File file : files) {
            xml = file.getAbsoluteFile();

            try {

                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(xml);
                xmlName = xml.getName();

                doc.getDocumentElement().normalize();

                NodeList nList = doc.getElementsByTagName("Article");
                if (nList.getLength() > 0) {
                    System.out.println(xml.getName() + " OLD");
                    for (int temp = 0; temp < nList.getLength(); temp++) {

                        Node nNode = nList.item(temp);

                        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                            buffer = new StringBuffer();
                            Element eElement = (Element) nNode;
                            String title = eElement.getElementsByTagName("title").item(0).getTextContent();
                            String keimeno = eElement.getElementsByTagName("content").item(0).getTextContent();
                            String link = eElement.getElementsByTagName("link").item(0).getTextContent();
                            String pubdate = eElement.getElementsByTagName("pubdate").item(0).getTextContent();
                            boolean classified = false;
                            int subjects = eElement.getElementsByTagName("subject").getLength();
                            if (subjects > 0) {
                                classified = true;
                            }

                            String article = title + (".") + keimeno;

                            if (article.contains("&quot;")) {
                                article = article.replaceAll("&quot;", "\"");
                                keimeno = keimeno.replaceAll("&quot;", "\"");
                                title = title.replaceAll("&quot;", "\"");
                            }
                            if (article.contains("&apos;")) {
                                article = article.replaceAll("&apos;", "'");
                                keimeno = keimeno.replaceAll("&apos;", "'");
                                title = title.replaceAll("&apos;", "'");
                            }

                            if (article.contains("&lt;")) {
                                article = article.replaceAll("&lt;", "<");
                                keimeno = keimeno.replaceAll("&lt;", "<");
                                title = title.replaceAll("&lt;", "<");
                            }

                            if (article.contains("&gt;")) {
                                article = article.replaceAll("&gt;", ">");
                                keimeno = keimeno.replaceAll("&gt;", ">");
                                title = title.replaceAll("&gt;", ">");
                            }

                            if (article.contains("&amp;")) {
                                article = article.replaceAll("&amp;", "&");
                                keimeno = keimeno.replaceAll("&amp;", "&");
                                title = title.replaceAll("&amp;", "&");
                            }

                            title = title.replaceAll("•", "").replaceAll("\\s{2,}", " ").replaceAll("(\\d+)(:)(\\d+)(,+)", "").replaceAll("(\\d+)(:)(\\d+)(.)(,+)", "").replaceAll("(\\/)(.{10,})(\\/)", "").replaceAll(" \\. ", " ");
                            keimeno = keimeno.replaceAll("•", "").replaceAll("\\s{2,}", " ").replaceAll("(\\d+)(:)(\\d+)(,+)", "").replaceAll("(\\d+)(:)(\\d+)(.)(,+)", "").replaceAll("(\\/)(.{10,})(\\/)", "").replaceAll(" \\. ", " ");
                            String rawKeimeno = keimeno;
                            Article arthro = new Article(link, title);
                            arthro.body = keimeno;

                            Classification c = new Classification();
                            double[] predictions = c.test(c, arthro);
                            if (predictions != null) {
                                System.out.println(Utils.arrayToString(predictions));
                            }
                            if (predictions != null) {
                                int flag = 0;
                                for (int j = 0; j < predictions.length; j++) {
                                    double percentage = Math.round(predictions[j] * 100);
                                    if (percentage > 0.5) {
                                        if (flag == 1) {
                                            arthro.category = arthro.category + ",";
                                        }
                                        arthro.category = arthro.category + classificationCategories.get(j);
                                        System.out.print(classificationCategories.get(j) + " " + percentage + ",");
                                        flag = 1;
                                    }
                                    if (j == predictions.length) {
                                        System.out.println();
                                    }
                                }
                                if (flag == 0) {
                                    System.out.println("--");
                                }
                            }
                            System.out.println();

                            arthro.category = arthro.category + "\t\tΕπιτυχής Κατηγοριοποίηση; (ΝΑΙ ΟΧΙ) ";

                            String subject = "";
                            if (classified == true) {
                                NamedNodeMap subs = eElement.getElementsByTagName("subject").item(0).getAttributes();
                                for (int i = 0; i < subs.getLength(); i++) {
                                    subject = subject + " " + subs.item(i).getTextContent();
                                }
                                arthro.category = subject;
                            }

                            keimeno = keimeno + "\n\nΜεταδεδομένα:\n\n" + "Σύνδεσμος στο άρθρο: \n" + link + "\n\nΗμερομ. δημοσίευσης: " + pubdate + "\n\nΘεματική: " + arthro.category + "\n\n";

                            article = article.replaceAll("•", "").replaceAll("\\s{2,}", " ").replaceAll("(\\d+)(:)(\\d+)(,+)", "").replaceAll("(\\d+)(:)(\\d+)(.)(,+)", "").replaceAll("(\\/)(.{10,})(\\/)", "").replaceAll(" \\. ", " ");
                            String plainText = article;
                            buffer.append(plainText);
                            Strings.add(buffer);
                            Word le_word = new Word();
                            le_word.keimeno = keimeno;
                            le_word.rawKeimeno = rawKeimeno;
                            le_word.rawXml = xml.getAbsolutePath();
                            le_word.link = link;
                            Keimena.add(le_word);
                            Xmls.add(xmlName);
                            Titles.add(title);
                            String editedArticle = article.replaceAll("[^\\p{IsLatin} -]", " ");

                            String[] articleArray = editedArticle.split("\\s+");

                            for (String tmp : articleArray) {
                                if (tmp != null && tmp.length() > 2) {
                                    String peza = tmp.toLowerCase(Locale.ENGLISH);
                                    if (!peza.contains("-")) {
                                        String nonAccented = stripAccents(peza);
                                        Word word = new Word(peza, nonAccented, tmp, xmlName, title, keimeno, xml.getAbsolutePath(), link, rawKeimeno);
                                        word.setClassified(classified);
                                        Words.add(word);
                                    } else if (peza.startsWith("-")) {
                                        String startHyphen = peza.replaceAll("-", "");
                                        String nonAccented = stripAccents(startHyphen);
                                        Word word = new Word(peza, nonAccented, tmp, xmlName, title, keimeno, xml.getAbsolutePath(), link, rawKeimeno);
                                        word.setClassified(classified);
                                        Words.add(word);
                                    } else if (peza.endsWith("-")) {
                                        String endHyphen = peza.replaceAll("-", "");
                                        String nonAccented = stripAccents(endHyphen);
                                        Word word = new Word(peza, nonAccented, tmp, xmlName, title, keimeno, xml.getAbsolutePath(), link, rawKeimeno);
                                        word.setClassified(classified);
                                        Words.add(word);
                                    } else {
                                        String nonAccented = stripAccents(peza);
                                        Word word = new Word(peza, nonAccented, tmp, xmlName, title, keimeno, xml.getAbsolutePath(), link, rawKeimeno);
                                        word.setClassified(classified);
                                        Words.add(word);
                                    }
                                }
                            }

                        }
                    }
                } else {
                    parseTeiEnglish();
                }
            } catch (ParserConfigurationException | IOException e) {
            } catch (SAXException ex) {
                Logger.getLogger(XmlReader.class.getName()).log(Level.SEVERE, null, ex);
            } catch (URISyntaxException ex) {
                Logger.getLogger(XmlReader.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(XmlReader.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void parseTeiEnglish() {
        try {

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xml);
            xmlName = xml.getName();

            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("TEI");
            if (nList.getLength() == 0) {
                System.out.println("Empty File");
                return;
            }
            System.out.println(xml.getName() + " TEI");
            for (int temp = 0; temp < nList.getLength(); temp++) {

                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    buffer = new StringBuffer();
                    Element eElement = (Element) nNode;
                    String title = "";
                    String keimeno = "";
                    String article = "";
                    String type = "";
                    String degree = "";
                    String edition = "";
                    String publisher = "";
                    String date = "";
                    String ref = "";
                    String subject = "";
                    String textType = "";

                    if (!eElement.getElementsByTagName("publisher").item(0).getAttributes().item(0).getNodeValue().isEmpty()) {
                        publisher = eElement.getElementsByTagName("publisher").item(0).getAttributes().item(0).getTextContent();
                    }

                    if (!eElement.getElementsByTagName("date").item(0).getAttributes().item(0).getNodeValue().isEmpty()) {
                        date = eElement.getElementsByTagName("date").item(0).getAttributes().item(0).getTextContent();
                    }
                    if (!eElement.getElementsByTagName("ref").item(0).getAttributes().item(0).getNodeValue().isEmpty()) {
                        ref = eElement.getElementsByTagName("ref").item(0).getAttributes().item(0).getTextContent();
                    }
                    if (!eElement.getElementsByTagName("ndc:subject").item(0).getAttributes().item(0).getNodeValue().isEmpty()) {
                        subject = eElement.getElementsByTagName("ndc:subject").item(0).getAttributes().item(0).getTextContent();
                    }

                    if (!eElement.getElementsByTagName("edition").item(0).getAttributes().item(0).getNodeValue().isEmpty()) {
                        edition = eElement.getElementsByTagName("edition").item(0).getAttributes().item(0).getTextContent();
                    }

                    if (eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeName().equals("type") && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeValue().isEmpty() && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(1).getNodeValue().isEmpty()) {
                        type = eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getTextContent();
                        degree = eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(1).getTextContent();
                    }

                    if (eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeName().equals("degree") && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeValue().isEmpty() && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(1).getNodeValue().isEmpty()) {
                        type = eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(1).getTextContent();
                        degree = eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getTextContent();
                    }

                    if (!eElement.getElementsByTagName("ndc:textType").item(0).getAttributes().item(0).getNodeValue().isEmpty()) {
                        textType = eElement.getElementsByTagName("ndc:textType").item(0).getAttributes().item(0).getTextContent();
                    }

                    int debug = 4;

                    if (eElement.getElementsByTagName("head").item(0).getAttributes().item(0).getNodeValue().equals("surTitle")) {
                        title = eElement.getElementsByTagName("head").item(1).getTextContent();
                        if (!eElement.getElementsByTagName("head").item(0).getTextContent().isEmpty()) {
                            keimeno = "\nΥπέρτιτλος: " + eElement.getElementsByTagName("head").item(0).getTextContent() + "\n\n";
                        }
                    } else {
                        title = eElement.getElementsByTagName("head").item(0).getTextContent();
                        article = eElement.getElementsByTagName("surTitle").item(0).getTextContent();
                        if (!eElement.getElementsByTagName("surTitle").item(0).getTextContent().isEmpty()) {
                            keimeno = "\nΥπέρτιτλος: " + eElement.getElementsByTagName("surTitle").item(0).getTextContent() + "\n\n";
                        }
                        debug = 3;
                    }

                    if (eElement.getElementsByTagName("head").item(2).getAttributes().item(0).getNodeValue().equals("subTitle") && !eElement.getElementsByTagName("head").item(2).getTextContent().isEmpty()) {
                        keimeno = keimeno + "\nΥπότιτλος: " + eElement.getElementsByTagName("head").item(2).getTextContent() + "\n\n";
                    }

                    for (int i = 0; i < eElement.getElementsByTagName("head").getLength(); i++) {
                        if (eElement.getElementsByTagName("head").item(i).getAttributes().item(0).getNodeValue().equals("midTitle") && !eElement.getElementsByTagName("head").item(i).getTextContent().isEmpty()) {
                            keimeno = keimeno + "\nΜεσότιτλος: " + eElement.getElementsByTagName("head").item(i).getTextContent() + "\n\n";
                        }
                    }

                    if (eElement.getElementsByTagName("head").item(debug).getAttributes().item(0).getNodeValue().equals("lead") && !eElement.getElementsByTagName("head").item(debug).getTextContent().isEmpty()) {
                        keimeno = keimeno + "\nΠαράγραφος Lead: " + eElement.getElementsByTagName("head").item(debug).getTextContent() + "\n\n";
                    }

                    for (int i = 0; i < eElement.getElementsByTagName("head").getLength(); i++) {
                        article = article + eElement.getElementsByTagName("head").item(i).getTextContent() + " ";
                    }

                    if (!eElement.getElementsByTagName("caption").item(0).getTextContent().isEmpty()) {
                        article = article + eElement.getElementsByTagName("caption").item(0).getTextContent() + " ";
                        keimeno = keimeno + "\nΛεζάντα: " + eElement.getElementsByTagName("caption").item(0).getTextContent() + "\n\n";
                    }

                    for (int y = 0; y < eElement.getElementsByTagName("p").getLength(); y++) {
                        article = article + eElement.getElementsByTagName("p").item(y).getTextContent() + " ";
                        keimeno = keimeno + eElement.getElementsByTagName("p").item(y).getTextContent() + " ";
                    }

                    if (article.contains("&quot;")) {
                        article = article.replaceAll("&quot;", "\"");
                        keimeno = keimeno.replaceAll("&quot;", "\"");
                        title = title.replaceAll("&quot;", "\"");
                    }
                    if (article.contains("&apos;")) {
                        article = article.replaceAll("&apos;", "'");
                        keimeno = keimeno.replaceAll("&apos;", "'");
                        title = title.replaceAll("&apos;", "'");
                    }

                    if (article.contains("&lt;")) {
                        article = article.replaceAll("&lt;", "<");
                        keimeno = keimeno.replaceAll("&lt;", "<");
                        title = title.replaceAll("&lt;", "<");
                    }

                    if (article.contains("&gt;")) {
                        article = article.replaceAll("&gt;", ">");
                        keimeno = keimeno.replaceAll("&gt;", ">");
                        title = title.replaceAll("&gt;", ">");
                    }

                    if (article.contains("&amp;")) {
                        article = article.replaceAll("&amp;", "&");
                        keimeno = keimeno.replaceAll("&amp;", "&");
                        title = title.replaceAll("&amp;", "&");
                    }

                    title = title.replaceAll("•", "").replaceAll("\\s{2,}", " ").replaceAll("(\\d+)(:)(\\d+)(,+)", "").replaceAll("(\\d+)(:)(\\d+)(.)(,+)", "").replaceAll("(\\/)(.{10,})(\\/)", "").replaceAll(" \\. ", " ");
                    keimeno = keimeno.replaceAll("•", "").replaceAll("(\\d+)(:)(\\d+)(,+)", "").replaceAll("(\\d+)(:)(\\d+)(.)(,+)", "").replaceAll("(\\/)(.{10,})(\\/)", "").replaceAll(" \\. ", " ");
                    String rawKeimeno = keimeno;
                    Article arthro = new Article(ref, title);
                    arthro.body = keimeno;
                    arthro.category = "";

                    Classification c = new Classification();
                    double[] predictions = c.test(c, arthro);
                    if (predictions != null) {
                        System.out.println(Utils.arrayToString(predictions));
                    }
                    if (predictions != null) {
                        int flag = 0;
                        for (int j = 0; j < predictions.length; j++) {
                            double percentage = Math.round(predictions[j] * 100);
                            if (percentage > 0.5) {
                                if (flag == 1) {
                                    arthro.category = arthro.category + ",";
                                }
                                arthro.category = arthro.category + classificationCategories.get(j);
                                System.out.print(classificationCategories.get(j) + " " + percentage + ",");
                                flag = 1;
                            }
                            if (j == predictions.length) {
                                System.out.println();
                            }
                        }
                        if (flag == 0) {
                            System.out.println("--");
                        }
                    }
                    System.out.println();

                    arthro.category = arthro.category + "\t\tΕπιτυχής Κατηγοριοποίηση; (ΝΑΙ ΟΧΙ) ";

                    keimeno = keimeno + "\n\nΜεταδεδομένα:\n\nΕφημερίδα: " + publisher + "\n\nΗμερομ. δημοσίευσης: " + date + "\n\nΣύνδεσμος στο άρθρο:  \n" + ref + "\n\nΜορφή έκδοσης: " + edition + "\n\nΘεματική: " + arthro.category + "\n\n";

                    if (eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeName().equals("type") && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeValue().isEmpty() && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(1).getNodeValue().isEmpty() && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeValue().equals(" ") && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(1).getNodeValue().equals(" ")) {
                        keimeno = keimeno + "Τρόπος λόγου: " + type + "  " + degree;
                    }

                    if (eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeName().equals("degree") && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeValue().isEmpty() && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(1).getNodeValue().isEmpty() && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(0).getNodeValue().equals(" ") && !eElement.getElementsByTagName("ndc:discourseMode").item(0).getAttributes().item(1).getNodeValue().equals(" ")) {
                        keimeno = keimeno + "Τρόπος λόγου: " + type + "  " + degree;
                    }

                    if (!eElement.getElementsByTagName("ndc:textType").item(0).getAttributes().item(0).getNodeValue().isEmpty() && !eElement.getElementsByTagName("ndc:textType").item(0).getAttributes().item(0).getNodeValue().equals(" ")) {
                        keimeno = keimeno + "\n\nΚειμενικός τύπος: " + textType + "\n\n";
                    }

                    article = article.replaceAll("•", "").replaceAll("\\s{2,}", " ").replaceAll("(\\d+)(:)(\\d+)(,+)", "").replaceAll("(\\d+)(:)(\\d+)(.)(,+)", "").replaceAll("(\\/)(.{10,})(\\/)", "").replaceAll(" \\. ", " ");

                    String plainText = article;
                    buffer.append(plainText);
                    Strings.add(buffer);
                    Word le_word = new Word();
                    le_word.keimeno = keimeno;
                    le_word.rawKeimeno = rawKeimeno;
                    le_word.rawXml = xml.getAbsolutePath();
                    le_word.link = ref;
                    Keimena.add(le_word);
                    Xmls.add(xmlName);
                    Titles.add(title);
                    String editedArticle = article.replaceAll("[^\\p{IsLatin} -]", " ");

                    String[] articleArray = editedArticle.split("\\s+");

                    for (String tmp : articleArray) {
                        if (tmp != null && tmp.length() > 2) {
                            String peza = tmp.toLowerCase(Locale.ENGLISH);
                            if (!peza.contains("-")) {
                                String nonAccented = stripAccents(peza);
                                Word word = new Word(peza, nonAccented, tmp, xmlName, title, keimeno, xml.getAbsolutePath(), ref, rawKeimeno);
                                Words.add(word);
                            } else if (peza.startsWith("-")) {
                                String startHyphen = peza.replaceAll("-", "");
                                String nonAccented = stripAccents(startHyphen);
                                Word word = new Word(peza, nonAccented, tmp, xmlName, title, keimeno, xml.getAbsolutePath(), ref, rawKeimeno);
                                Words.add(word);
                            } else if (peza.endsWith("-")) {
                                String endHyphen = peza.replaceAll("-", "");
                                String nonAccented = stripAccents(endHyphen);
                                Word word = new Word(peza, nonAccented, tmp, xmlName, title, keimeno, xml.getAbsolutePath(), ref, rawKeimeno);
                                Words.add(word);
                            } else {
                                String nonAccented = stripAccents(peza);
                                Word word = new Word(peza, nonAccented, tmp, xmlName, title, keimeno, xml.getAbsolutePath(), ref, rawKeimeno);
                                Words.add(word);
                            }
                        }
                    }
                }
            }
        } catch (ParserConfigurationException | IOException e) {
        } catch (SAXException ex) {
            Logger.getLogger(XmlReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(XmlReader.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(XmlReader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String stripAccents(final String input) {

        final Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");//$NON-NLS-1$

        final String decomposed = Normalizer.normalize(input, Normalizer.Form.NFD);

        return pattern.matcher(decomposed).replaceAll("");//$NON-NLS-1$

    }
}
