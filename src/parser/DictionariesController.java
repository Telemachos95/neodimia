/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import static parser.GetPropertyValues.address;
import static parser.GetPropertyValues.password;
import static parser.GetPropertyValues.port;
import static parser.GetPropertyValues.username;
import static parser.XmlReader.stripAccents;
import static gui.MainFrame.DbWords;
import static gui.MainFrame.Words;
import static gui.MainFrame.neologismoi;
import static gui.MainFrame.original;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Tilemachos
 */
public class DictionariesController {

    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;
    private Connection connection = null;

    public void DatabaseConnector() throws IOException, InterruptedException {

        String url = "jdbc:mysql://" + address + ":" + port + "/ci";
        int counter = 0;

        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException ex) {
            System.out.println(ex);
            return;
        }

        try {

            connection = DriverManager.getConnection(url, username, password);

            preparedStatement = connection
                    .prepareStatement("select lima from data");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String lima = resultSet.getString("lima");
                lima = stripAccents(lima);
                DbWords.add(lima.toLowerCase(new java.util.Locale("el")));
                counter++;
            }
            connection.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

        String url1 = "jdbc:mysql://" + address + ":" + port + "/ci2";

        try {

            connection = DriverManager.getConnection(url1, username, password);

            preparedStatement = connection
                    .prepareStatement("select lima from dic");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String lima = resultSet.getString("lima");
                lima = stripAccents(lima);
                DbWords.add(lima.toLowerCase(new java.util.Locale("el")));
                counter++;
            }
            connection.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

        try {

            connection = DriverManager.getConnection(url1, username, password);

            preparedStatement = connection
                    .prepareStatement("select organismos from organismoi");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String lima = resultSet.getString("organismos");
                lima = stripAccents(lima);
                DbWords.add(lima.toLowerCase(new java.util.Locale("el")));
                counter++;
            }
            connection.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

        try {

            connection = DriverManager.getConnection(url1, username, password);

            preparedStatement = connection
                    .prepareStatement("select proion from proionta");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String lima = resultSet.getString("proion");
                lima = stripAccents(lima);
                DbWords.add(lima.toLowerCase(new java.util.Locale("el")));
                counter++;
            }
            connection.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

        try {

            connection = DriverManager.getConnection(url1, username, password);

            preparedStatement = connection
                    .prepareStatement("select proswpo from proswpa");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String lima = resultSet.getString("proswpo");
                lima = stripAccents(lima);
                DbWords.add(lima.toLowerCase(new java.util.Locale("el")));
                counter++;
            }
            connection.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

        try {

            connection = DriverManager.getConnection(url1, username, password);

            preparedStatement = connection
                    .prepareStatement("select topothesia from topothesies");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String lima = resultSet.getString("topothesia");
                lima = stripAccents(lima);
                DbWords.add(lima.toLowerCase(new java.util.Locale("el")));
                counter++;
            }
            connection.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

        try {

            connection = DriverManager.getConnection(url1, username, password);

            preparedStatement = connection
                    .prepareStatement("select lima from tomoi");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String lima = resultSet.getString("lima");
                lima = stripAccents(lima);
                DbWords.add(lima.toLowerCase(new java.util.Locale("el")));
                counter++;
            }
            connection.close();
        } catch (SQLException e) {
            System.out.println(e);
        }

        try {

            connection = DriverManager.getConnection(url1, username, password);

            preparedStatement = connection
                    .prepareStatement("select lima from errors");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String lima = resultSet.getString("lima");
                lima = stripAccents(lima);
                DbWords.add(lima.toLowerCase(new java.util.Locale("el")));
                counter++;
            }
            connection.close();
        } catch (SQLException e) {
            System.out.println(e);
        }
        //System.out.println(counter);
    }

    public void NewWordFinder() {

        Set<String> set = new LinkedHashSet<>();
        ArrayList<Word> noDuplicates = new ArrayList<>();

        for (Word item : Words) {
            if (set.add(item.getNonAccentedWord())) {
                noDuplicates.add(item);
            }
        }

        for (Word word : noDuplicates) {
            int count = 0;
            for (String lima : DbWords) {
                if (word.getNonAccentedWord() != null && lima != null && word.getNonAccentedWord().equals(lima)) {
                    count++;
                    break;
                }

            }
            if (count == 0) {
                String temp = XmlReader.stripAccents(word.getOriginalWord().toUpperCase(new java.util.Locale("el")));
                neologismoi.add(temp);
                original.add(word.originalWord);
//                System.out.println(word.getAccentedWord());
            }
        }

    }

    public int AddLimma(String lima) {

        String url = "jdbc:mysql://" + address + ":" + port + "/ci2?useUnicode=true&characterEncoding=utf-8";
        int i = 0;

        try {

            connection = DriverManager.getConnection(url, username, password);

            preparedStatement = connection
                    .prepareStatement("insert into dic (lima) values (?)");
            preparedStatement.setString(1, lima);
            i = preparedStatement.executeUpdate();
            System.out.println(i);
            if (i > 0) {
                System.out.println("its ok");
            } else {
                System.out.println("error");
            }
            connection.close();
        } catch (SQLException e) {
            Logger.getLogger(DictionariesController.class.getName()).log(Level.SEVERE, null, e);
        }
        return i;
    }

    public int AddLimmaErrors(String lima) {

        String url = "jdbc:mysql://" + address + ":" + port + "/ci2?useUnicode=true&characterEncoding=utf-8";
        int i = 0;

        try {

            connection = DriverManager.getConnection(url, username, password);

            preparedStatement = connection
                    .prepareStatement("insert into errors (lima) values (?)");
            preparedStatement.setString(1, lima);
            i = preparedStatement.executeUpdate();
            System.out.println(i);
            if (i > 0) {
                System.out.println("its ok");
            } else {
                System.out.println("error");
            }
            connection.close();
        } catch (SQLException e) {
            Logger.getLogger(DictionariesController.class.getName()).log(Level.SEVERE, null, e);
        }
        return i;
    }

}
