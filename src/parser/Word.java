/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser;

import static gui.MainFrame.RawWord;
import static gui.MainFrame.Sentences;
import static gui.MainFrame.Strings;
import static gui.MainFrame.Words;
import static gui.MainFrame.keimena;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Tilemachos
 */
public class Word {

    public String accentedWord;
    public String nonAccentedWord;
    public String originalWord;
    public String xml;
    public String rawXml;
    public String title;
    public String keimeno;
    public String rawKeimeno;
    public boolean classified;
    public int arrayId;
    public String link;

    public Word(String accentedWord, String nonAccentedWord) {
        this.accentedWord = accentedWord;
        this.nonAccentedWord = nonAccentedWord;
        classified = false;
    }

    public Word(String accentedWord, String nonAccentedWord, String originalWord) {
        this.accentedWord = accentedWord;
        this.nonAccentedWord = nonAccentedWord;
        this.originalWord = originalWord;
        classified = false;
    }

    public Word(String accentedWord, String nonAccentedWord, String originalWord, String xml, String title, String keimeno, String rawXml, String link, String rawKeimeno) {
        this.accentedWord = accentedWord;
        this.nonAccentedWord = nonAccentedWord;
        this.originalWord = originalWord;
        this.xml = xml;
        this.rawXml = rawXml;
        this.title = title;
        this.keimeno = keimeno;
        this.rawKeimeno = rawKeimeno;
        classified = false;
        this.link = link;
    }

    public Word() {
        classified = false;
    }

    public String getRawKeimeno() {
        return rawKeimeno;
    }

    public void setRawKeimeno(String rawKeimeno) {
        this.rawKeimeno = rawKeimeno;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getRawXml() {
        return rawXml;
    }

    public void setRawXml(String rawXml) {
        this.rawXml = rawXml;
    }

    public int getArrayId() {
        return arrayId;
    }

    public void setArrayId(int arrayId) {
        this.arrayId = arrayId;
    }

    public boolean isClassified() {
        return classified;
    }

    public void setClassified(boolean classified) {
        this.classified = classified;
    }

    public String getKeimeno() {
        return keimeno;
    }

    public void setKeimeno(String keimeno) {
        this.keimeno = keimeno;
    }

    public String getOriginalWord() {
        return originalWord;
    }

    public void setOriginalWord(String originalWord) {
        this.originalWord = originalWord;
    }

    public static ArrayList<String> getSentences() {
        return Sentences;
    }

    public String getXml() {
        return xml;
    }

    public void setXml(String xml) {
        this.xml = xml;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAccentedWord() {
        return accentedWord;
    }

    public void setAccentedWord(String accentedWord) {
        this.accentedWord = accentedWord;
    }

    public String getNonAccentedWord() {
        return nonAccentedWord;
    }

    public void setNonAccentedWord(String nonAccentedWord) {
        this.nonAccentedWord = nonAccentedWord;
    }

    public void sentenceFinder(String word) {

        BreakIterator iterator = BreakIterator.getWordInstance(new java.util.Locale("el"));
        String surr = "";
        String lima;

        for (StringBuffer buffer : Strings) {

            String allText = buffer.toString();
            String allWords = "";
            iterator.setText(allText);

            int start = iterator.first();
            for (int end = iterator.next();
                    end != BreakIterator.DONE;
                    start = end, end = iterator.next()) {
                lima = buffer.substring(start, end);
                allWords = allWords + lima;
            }

            String[] arr = allWords.split(" +");

            for (int i = 0; i < arr.length; i++) {
                String unAccented = XmlReader.stripAccents(word);
                String unAccented1 = XmlReader.stripAccents(arr[i]);
                String regex = ".*\\b" + unAccented + "\\b.*";
                Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
                Matcher matcher = pattern.matcher(unAccented1);

                if (matcher.find()) {
                    surr = "";

                    // have to check for ArrayIndexOutOfBoundsException
                    surr = (i - 10 > 0 ? arr[i - 10] + " " : "")
                            + (i - 9 > 0 ? arr[i - 9] + " " : "")
                            + (i - 8 > 0 ? arr[i - 8] + " " : "")
                            + (i - 7 > 0 ? arr[i - 7] + " " : "")
                            + (i - 6 > 0 ? arr[i - 6] + " " : "")
                            + (i - 5 > 0 ? arr[i - 5] + " " : "")
                            + (i - 4 > 0 ? arr[i - 4] + " " : "")
                            + (i - 3 > 0 ? arr[i - 3] + " " : "")
                            + (i - 2 > 0 ? arr[i - 2] + " " : "")
                            + (i - 1 > 0 ? arr[i - 1] + " " : "")
                            + arr[i]
                            + (i + 1 < arr.length ? " " + arr[i + 1] : "")
                            + (i + 2 < arr.length ? " " + arr[i + 2] : "")
                            + (i + 3 < arr.length ? " " + arr[i + 3] : "")
                            + (i + 4 < arr.length ? " " + arr[i + 4] : "")
                            + (i + 5 < arr.length ? " " + arr[i + 5] : "")
                            + (i + 6 < arr.length ? " " + arr[i + 6] : "")
                            + (i + 7 < arr.length ? " " + arr[i + 7] : "")
                            + (i + 8 < arr.length ? " " + arr[i + 8] : "")
                            + (i + 9 < arr.length ? " " + arr[i + 9] : "")
                            + (i + 10 < arr.length ? " " + arr[i + 10] : "");

                    Sentences.add(surr);
                    String originalSentence = surr;
                    String nonAccentedSentence = XmlReader.stripAccents(surr);
                    String nonAccented = XmlReader.stripAccents(word);
                    int originalWordIndex = nonAccentedSentence.toLowerCase(new java.util.Locale("el")).indexOf(nonAccented.toLowerCase(new java.util.Locale("el")));
                    while (originalWordIndex >= 0) {
                        String original = originalSentence.substring(originalWordIndex, originalWordIndex + word.length());
                        RawWord.add(original);
                        originalWordIndex = nonAccentedSentence.toLowerCase(new java.util.Locale("el")).indexOf(nonAccented.toLowerCase(new java.util.Locale("el")), originalWordIndex + 1);
                    }

                }
            }

        }
    }

    public void wordMatcher(String word) {

        for (int i = 0; i < Words.size(); i++) {

            Word neologismos = new Word();

            String regex = ".*\\b" + word + "\\b.*";

            Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);
            Matcher matcher = pattern.matcher(Words.get(i).getOriginalWord());

            if (matcher.find()) {
                neologismos.setKeimeno(Words.get(i).getKeimeno());
                neologismos.setTitle(Words.get(i).getTitle());
                neologismos.setXml(Words.get(i).getXml());
                neologismos.setArrayId(i);
                keimena.add(neologismos);
                String originalText = Words.get(i).getKeimeno();
                String nonAccented = XmlReader.stripAccents(word);
                String nonAccentedText = XmlReader.stripAccents(originalText);
                int originalWordIndex = nonAccentedText.toLowerCase(new java.util.Locale("el")).indexOf(nonAccented.toLowerCase(new java.util.Locale("el")));
                while (originalWordIndex >= 0) {
                    String original = originalText.substring(originalWordIndex, originalWordIndex + word.length());
                    RawWord.add(original);
                    originalWordIndex = nonAccentedText.toLowerCase(new java.util.Locale("el")).indexOf(nonAccented.toLowerCase(new java.util.Locale("el")), originalWordIndex + 1);
                }
            }

        }
    }

}
