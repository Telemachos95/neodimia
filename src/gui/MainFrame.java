/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import article_classification.Article;
import parser.GetPropertyValues;
import parser.DictionariesController;
import parser.EnglishStemmer;
import parser.FileListAccessory;
import static parser.GetPropertyValues.address;
import static parser.GetPropertyValues.password;
import static parser.GetPropertyValues.port;
import static parser.GetPropertyValues.username;
import parser.ListExporter;
import parser.Menu;
import parser.SimilarWord;
import parser.Stemmer;
import parser.TrainingData;
import parser.Word;
import parser.XmlReader;
import parser.XmlWriter;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.Collator;
import java.text.Normalizer;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import static javax.swing.text.StyleConstants.Foreground;
import javax.swing.text.StyledDocument;
import static parser.GetPropertyValues.serverPort;

/**
 *
 * @author Tilemachos
 */
public class MainFrame extends javax.swing.JFrame {

    public static ArrayList<DefaultListModel> modelList = new ArrayList<>();
    public static ArrayList<String> DbWords = new ArrayList<>();
    public static ArrayList<String> neologismoi = new ArrayList<>();
    public static ArrayList<String> original = new ArrayList<>();
    public static ArrayList<Word> Words = new ArrayList<>();
    public static ArrayList<String> Sentences = new ArrayList<>();
    public static ArrayList<Word> keimena = new ArrayList<>();
    public static ArrayList<String> RawWord = new ArrayList<>();
    public static ArrayList<StringBuffer> Strings = new ArrayList();
    public static ArrayList<Word> Keimena = new ArrayList();
    public static ArrayList<String> Xmls = new ArrayList();
    public static ArrayList<String> Titles = new ArrayList();
    public static ArrayList<String> stems = new ArrayList();
    public static ArrayList<SimilarWord> alternatives = new ArrayList();
    public static ArrayList<Integer> appearances = new ArrayList();
    public static ArrayList<Integer> articleNumbers = new ArrayList();
    public static ArrayList<TrainingData> trainingData = new ArrayList();
    public static HashSet<String> stopwords = new HashSet();
    public static ArrayList<String> classificationCategories = new ArrayList();

    JFrame frame2;
    JFrame frame3;
    Neologismos sp = new Neologismos();
    Leksiko leksiko = new Leksiko();
    Keimeno keimeno = new Keimeno();
    Keimeno keimeno1 = new Keimeno();
    WordsPanel wordsPanel = new WordsPanel();
    Word neologismos;
    boolean active = false;
    int panel = 1;
    int list = 0;
    int counter = 0;
    int wordCounter = 0;
    int location = 0;
    int sentenceIndex = 0;
    int value;
    int wordIndex;
    int originalIndex = 0;

    public MainFrame() throws IOException {

        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
        }

        initComponents();
        setTitle("Νεοδημία - Κέντρον Ερεύνης Επιστημονικών Όρων και Νεολογισμών");
        setSize(815, 800);
        setLocationRelativeTo(null);
        setVisible(true);
        getContentPane().setBackground(new java.awt.Color(219, 213, 213));
        jButton2.setEnabled(false);
        jButton7.setEnabled(false);
        jLabel4.setVisible(false);
        jLabel4.setText("");
        jTextArea1.setEditable(false);
        jTextArea1.setWrapStyleWord(true);
        jButton4.setBackground(new java.awt.Color(255, 255, 255));

        Image img;
        try {
            img = ImageIO.read(getClass().getResource("/open.png"));
            jButton1.setIcon(new ImageIcon(img));
            jButton1.setFocusPainted(false);
        } catch (IOException ex) {
            Logger.getLogger(Neologismos.class.getName()).log(Level.SEVERE, null, ex);
        }

        sp.jButton1.addActionListener(new event1());
        sp.jButton2.addActionListener(new event5());
        sp.jTextPane1.addMouseListener(new event2());
        Menu contextMenu = new Menu();
        contextMenu.add(sp.jTextPane1);
        wordsPanel.jButton1.addActionListener(new event4());
        keimeno.jButton1.addActionListener(new event6());
        keimeno1.jButton1.addActionListener(new event9());
        keimeno.jTextPane1.addMouseListener(new event7());
        keimeno1.jTextPane1.addMouseListener(new event10());
        keimeno.jTextPane1.addMouseListener(new event11());
        keimeno1.jTextPane1.addMouseListener(new event12());
        Menu contextMenu1 = new Menu();
        contextMenu1.add(keimeno.jTextPane1);
        contextMenu1.add(keimeno1.jTextPane1);

        sp.setBackground(new java.awt.Color(219, 213, 213));
        wordsPanel.setBackground(new java.awt.Color(219, 213, 213));
        keimeno.setBackground(new java.awt.Color(219, 213, 213));
        keimeno1.setBackground(new java.awt.Color(219, 213, 213));

        GetPropertyValues properties = new GetPropertyValues();
        properties.getPropValues();

        try {
            img = ImageIO.read(getClass().getResource("/options.png"));
            jButton6.setIcon(new ImageIcon(img));
            jButton6.setToolTipText("Οι ρυθμίσεις για την σύνδεση στην βάση");
            jButton6.setFocusPainted(false);
            jButton6.addActionListener(new event3());
        } catch (IOException ex) {
            Logger.getLogger(Neologismos.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            img = ImageIO.read(getClass().getResource("/save.gif"));
            jButton7.setIcon(new ImageIcon(img));
            jButton7.setFocusPainted(false);
        } catch (IOException ex) {
            Logger.getLogger(Neologismos.class.getName()).log(Level.SEVERE, null, ex);
        }

        jTable1.setEnabled(false);
        jTable1.getTableHeader().setReorderingAllowed(false);

        jTable1.getTableHeader().setFont(new Font("Tahoma", Font.PLAIN, 13));
        jTable1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            @Override
            public void mouseMoved(java.awt.event.MouseEvent e) {
                int row = jTable1.rowAtPoint(e.getPoint());
                int col = jTable1.columnAtPoint(e.getPoint());
                String value = "";
                if (col != 4 && col != 5) {
                    value = jTable1.getValueAt(row, col).toString();
                }
                if (!value.equals(null) && !value.equals("")) {
                    jTable1.setToolTipText(value);
                }
            }
        });

        Scanner s = new Scanner(new File(System.getProperty("user.dir") + "/resources/stopwords.txt"));
        while (s.hasNext()) {
            stopwords.add(s.next());
        }
        s.close();

        classificationCategories.add("Ελλάδα: Πολιτική");
        classificationCategories.add("Ελλάδα: Κοινωνία");
        classificationCategories.add("Ελλάδα: Οικονομία");
        classificationCategories.add("Διεθνή: Πολιτική");
        classificationCategories.add("Διεθνή: Κοινωνία");
        classificationCategories.add("Διεθνή: Οικονομία");
        classificationCategories.add("Επιστήμη και Τεχνολογία");
        classificationCategories.add("Πολιτισμός");
        classificationCategories.add("Αθλητισμός");
        classificationCategories.add("Βιοτροπία: Δραστηριότητες Ελεύθερου Χρόνου");
        classificationCategories.add("Βιοτροπία: Διατροφή – Σώμα");
        classificationCategories.add("Βιοτροπία: Κοινωνικά – Κοσμικά – Τηλεοπτικά Νέα");
        classificationCategories.add("Κατανάλωση: Σπίτι – Οικογένεια – Εμφάνιση ");
        classificationCategories.add("Κατανάλωση: Εμπορική Τεχνολογία");
        classificationCategories.add("Κατανάλωση: Αυτοκίνηση");
        classificationCategories.add("Ξενόγλωσσα");
        classificationCategories.add("Άλλο");

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jToolBar1 = new javax.swing.JToolBar();
        jButton4 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(java.awt.Color.lightGray);
        setIconImage((Toolkit.getDefaultToolkit().getImage(getClass().getResource("/logo.png"))));

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        jButton2.setText("Αναζήτηση  Νεολογισμών");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel4.setMaximumSize(new java.awt.Dimension(10, 10));

        jButton4.setBackground(new java.awt.Color(255, 153, 102));
        jButton4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton4.setText("Αναζήτηση Νεολογισμών");
        jButton4.setFocusable(false);
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton4.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton4);

        jButton3.setBackground(new java.awt.Color(255, 153, 102));
        jButton3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton3.setText("Γρήγορη Προσθήκη");
        jButton3.setFocusable(false);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton3);

        jButton5.setBackground(new java.awt.Color(255, 153, 102));
        jButton5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton5.setText("Αναζήτηση Ξενόγλωσσων");
        jButton5.setFocusable(false);
        jButton5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton5.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton5);

        jButton9.setBackground(new java.awt.Color(255, 153, 102));
        jButton9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton9.setText("Κατηγοριοποίηση Άρθρων");
        jButton9.setFocusable(false);
        jButton9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton9.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton9);

        jButton7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jTextArea1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTextArea1.setMaximumSize(new java.awt.Dimension(6, 23));
        jTextArea1.setMinimumSize(new java.awt.Dimension(6, 23));
        jScrollPane3.setViewportView(jTextArea1);

        jTable1.setFont(new java.awt.Font("Tahoma", 0, 15)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Βάση (μέσω stemming)", "Τύποι"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class

                , java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

        });
        jScrollPane2.setViewportView(jTable1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 555, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(57, 57, 57))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE)
                        .addGap(33, 33, 33)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27))
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 740, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(294, 294, 294))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(350, 350, 350))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        modelList.clear();
        if (panel == 1) {
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Αρχεία XML", "xml");
            JFileChooser fileChooser = new JFileChooser();   //Used for choosing the file.
            FileListAccessory accessory = new FileListAccessory(fileChooser);
            fileChooser.setAccessory(accessory);
            fileChooser.setCurrentDirectory(new File(System.getProperty("user.home"))); //The file selection will start from the home directory.
            fileChooser.setFileFilter(filter);
            fileChooser.setDialogTitle("Άνοιγμα Αρχείου");
            fileChooser.setMultiSelectionEnabled(true);
            fileChooser.setDragEnabled(true);
            fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            Component frame = null;
            int result = fileChooser.showOpenDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) { //Check if the user selected a file.
                DefaultListModel model = accessory.getModel();
                modelList.add(model);
                jButton2.setEnabled(true);
                jTextArea1.setText(model.toString().replaceAll("[\\[\\]]", "").replaceAll("\\s", "").replaceAll(",", "\n"));

            }
        } else {
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Αρχεία XML", "xml");
            JFileChooser fileChooser = new JFileChooser();   //Used for choosing the file.   
            FileListAccessory accessory = new FileListAccessory(fileChooser);
            fileChooser.setAccessory(accessory);
            fileChooser.setCurrentDirectory(new File(System.getProperty("user.home"))); //The file selection will start from the home directory.
            fileChooser.setFileFilter(filter);
            fileChooser.setDialogTitle("Άνοιγμα Αρχείου");
            fileChooser.setMultiSelectionEnabled(true);
            fileChooser.setDragEnabled(true);
            fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            Component frame = null;
            int result = fileChooser.showOpenDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) { //Check if the user selected a file.
                DefaultListModel model = accessory.getModel();
                modelList.add(model);
                jButton2.setEnabled(true);
                jTextArea1.setText(model.toString().replaceAll("[\\[\\]]", "").replaceAll("\\s", "").replaceAll(",", "\n"));

            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        wordCounter = 0;
        neologismoi.clear();
        Words.clear();
        Sentences.clear();
        original.clear();
        Strings.clear();
        stems.clear();
        alternatives.clear();
        appearances.clear();
        articleNumbers.clear();
        RawWord.clear();
        Keimena.clear();
        Titles.clear();
        Xmls.clear();
        jTable1.removeAll();
        wordsPanel.jTable1.removeAll();

        MouseListener[] mls = jTable1.getMouseListeners();
        for (MouseListener ml : mls) {
            jTable1.removeMouseListener(ml);
        }
        MouseListener[] mls1 = wordsPanel.jTable1.getMouseListeners();
        for (MouseListener ml : mls1) {
            wordsPanel.jTable1.removeMouseListener(ml);
        }

        jButton1.setEnabled(false);
        jButton2.setEnabled(false);
        jButton3.setEnabled(false);
        jButton4.setEnabled(false);
        jButton5.setEnabled(false);
        jButton6.setEnabled(false);
        jButton7.setEnabled(false);
        jButton9.setEnabled(false);
        URL imageURL = this.getClass().getClassLoader().getResource("loader.gif");
        Icon myImgIcon = new ImageIcon(imageURL);
        jLabel4.setIcon(myImgIcon);
        jLabel4.setVisible(true);

        if (panel == 1) {
            SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() {
                    list = 1;
                    XmlReader reader = new XmlReader();
                    reader.parseXml();
                    DictionariesController dic = new DictionariesController();
                    try {
                        dic.DatabaseConnector();
                    } catch (IOException ex) {
                        Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    dic.NewWordFinder();
                    Collator coll = Collator.getInstance(new java.util.Locale("el"));
                    coll.setStrength(Collator.PRIMARY);
                    Collections.sort(neologismoi, coll);
                    Collections.sort(original, coll);

                    Stemmer stemmer = new Stemmer();
                    Set<String> StemSet = new LinkedHashSet<>();

                    int result;
                    for (String test : neologismoi) {
                        test = test.toLowerCase(new java.util.Locale("el"));
                        test = test.replaceAll("ς", "σ");
                        final Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");//$NON-NLS-1$
                        final String decomposed = Normalizer.normalize(test, Normalizer.Form.NFD);
                        test = pattern.matcher(decomposed).replaceAll("");//$NON-NLS-1$
                        result = stemmer.stem(test.toCharArray(), test.toCharArray().length);
                        test = test.substring(0, result).toUpperCase(new java.util.Locale("el"));

                        if (StemSet.add(test)) {
                            stems.add(test);
                        }
                    }

                    Set<Integer> SimilarSet = new LinkedHashSet<>();
                    boolean isSimilar = false;

                    for (int i = 0; i < neologismoi.size(); i++) {
                        if (!SimilarSet.isEmpty()) {
                            for (Integer index : SimilarSet) {
                                if (index == i) {
                                    isSimilar = true;
                                }
                            }
                        }

                        if (isSimilar == true) {
                            isSimilar = false;
                            continue;
                        }

                        SimilarWord similar = new SimilarWord();
                        similar.similarWords = neologismoi.get(i);

                        String temp;
                        temp = neologismoi.get(i).toLowerCase(new java.util.Locale("el"));
                        temp = temp.replaceAll("ς", "σ");
                        final Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");//$NON-NLS-1$
                        final String decomposed = Normalizer.normalize(temp, Normalizer.Form.NFD);
                        temp = pattern.matcher(decomposed).replaceAll("");//$NON-NLS-1$
                        result = stemmer.stem(temp.toCharArray(), temp.toCharArray().length);
                        temp = temp.substring(0, result).toUpperCase(new java.util.Locale("el"));

                        for (int j = 0; j < neologismoi.size(); j++) {
                            if (neologismoi.get(i) == null ? neologismoi.get(j) == null : neologismoi.get(i).equals(neologismoi.get(j))) {
                                continue;
                            }

                            String temp1;
                            temp1 = neologismoi.get(j).toLowerCase(new java.util.Locale("el"));
                            temp1 = temp1.replaceAll("ς", "σ");
                            final Pattern pattern1 = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");//$NON-NLS-1$
                            final String decomposed1 = Normalizer.normalize(temp1, Normalizer.Form.NFD);
                            temp1 = pattern1.matcher(decomposed1).replaceAll("");//$NON-NLS-1$
                            result = stemmer.stem(temp1.toCharArray(), temp1.toCharArray().length);
                            temp1 = temp1.substring(0, result).toUpperCase(new java.util.Locale("el"));

                            if (temp.equals(temp1)) {
                                similar.similarWords = similar.similarWords + " " + neologismoi.get(j);
                                SimilarSet.add(j);
                            }
                        }
                        alternatives.add(similar);
                    }

                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    dtm.setRowCount(stems.size());
                    jTable1.setModel(dtm);

                    DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
                    centerRenderer.setHorizontalAlignment(JLabel.CENTER);
                    jTable1.setDefaultRenderer(String.class, centerRenderer);

                    int listIndex;

                    listIndex = 0;
                    for (String stem : stems) {
                        jTable1.setValueAt(stem, listIndex, 0);
                        listIndex++;
                    }

                    listIndex = 0;
                    for (SimilarWord words : alternatives) {
                        jTable1.setValueAt(words.getSimilarWords(), listIndex, 1);
                        listIndex++;
                    }

                    TableRowSorter<TableModel> sorter = new TableRowSorter<>(jTable1.getModel());
                    jTable1.setRowSorter(sorter);
                    List<RowSorter.SortKey> sortKeys = new ArrayList<>();
                    sorter.setSortKeys(sortKeys);

                    jButton1.setEnabled(true);
                    jButton2.setEnabled(true);
                    jButton3.setEnabled(true);
                    jButton4.setEnabled(true);
                    jButton5.setEnabled(true);
                    jButton6.setEnabled(true);
                    jButton7.setEnabled(true);
                    jButton9.setEnabled(true);
                    jTable1.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent evt) {
                            JTable table = (JTable) evt.getSource();
                            if (evt.getClickCount() == 2) {
                                Point p = evt.getPoint();
                                int index = table.rowAtPoint(p);
                                jTextArea1.setVisible(false);
                                jScrollPane3.setVisible(false);
                                jButton3.setVisible(false);
                                jButton4.setVisible(false);
                                jButton5.setVisible(false);
                                jButton7.setVisible(false);
                                jButton9.setVisible(false);
                                jButton1.setVisible(false);
                                jLabel4.setVisible(false);
                                jButton2.setVisible(false);
                                jTextArea1.setVisible(false);
                                jToolBar1.setVisible(false);
                                jScrollPane3.setVisible(false);
                                jScrollPane2.setVisible(false);
                                jTable1.setVisible(false);
                                wordsPanel.setSize(800, 800);
                                wordsPanel.setBackground(new java.awt.Color(219, 213, 213));

                                wordsPanel.jTable1.setEnabled(false);
                                wordsPanel.jTable1.getTableHeader().setReorderingAllowed(false);

                                wordsPanel.jTable1.getTableHeader().setFont(new Font("Tahoma", Font.PLAIN, 13));
                                wordsPanel.jTable1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
                                    @Override
                                    public void mouseMoved(java.awt.event.MouseEvent e) {
                                        int row = wordsPanel.jTable1.rowAtPoint(e.getPoint());
                                        int col = wordsPanel.jTable1.columnAtPoint(e.getPoint());
                                        String value = "";
                                        if (col != 4 && col != 5) {
                                            value = wordsPanel.jTable1.getValueAt(row, col).toString();
                                        }
                                        if (!value.equals(null) && !value.equals("")) {
                                            wordsPanel.jTable1.setToolTipText(value);
                                        }
                                    }
                                });

                                wordsPanel.jLabel2.setText(jTable1.getValueAt(index, 0).toString());
                                int listIndex = 0;
                                String allWords = jTable1.getValueAt(index, 1).toString();
                                String[] splitStr = allWords.split("\\s+");
                                DefaultTableModel dtm = (DefaultTableModel) wordsPanel.jTable1.getModel();
                                dtm.setRowCount(splitStr.length);
                                wordsPanel.jTable1.setModel(dtm);
                                for (int i = 0; i < splitStr.length; i++) {
                                    wordsPanel.jTable1.setValueAt(splitStr[i], i, 0);
                                }

                                int emfaniseis;
                                int articleNumber;
                                Set<String> set = new LinkedHashSet<>();

                                for (int i = 0; i < splitStr.length; i++) {
                                    emfaniseis = 0;
                                    articleNumber = 0;
                                    set.clear();
                                    for (Word test2 : Words) {
                                        String lima = test2.nonAccentedWord.toUpperCase(new java.util.Locale("el"));
                                        if (splitStr[i].equals(lima)) {
                                            if (set.add(test2.title)) {
                                                articleNumber++;
                                            }
                                            emfaniseis++;
                                        }
                                    }
                                    appearances.add(emfaniseis);
                                    articleNumbers.add(articleNumber);
                                }

                                for (int i = 0; i < splitStr.length; i++) {
                                    wordsPanel.jTable1.setValueAt(appearances.get(i), i, 1);
                                }

                                for (int i = 0; i < splitStr.length; i++) {
                                    wordsPanel.jTable1.setValueAt(articleNumbers.get(i), i, 2);
                                }

                                appearances.clear();
                                articleNumbers.clear();

                                DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
                                centerRenderer.setHorizontalAlignment(JLabel.CENTER);
                                wordsPanel.jTable1.setDefaultRenderer(String.class, centerRenderer);

                                add(wordsPanel);
                                wordsPanel.setVisible(true);
                            }
                        }
                    });

                    wordsPanel.jTable1.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent evt) {
                            JTable table = (JTable) evt.getSource();
                            if (evt.getClickCount() == 2) {
                                try {
                                    Point p = evt.getPoint();
                                    int index = table.rowAtPoint(p);
                                    wordsPanel.setVisible(false);
                                    sp.setVisible(true);
                                    sp.setSize(800, 800);
                                    sp.setBackground(new java.awt.Color(219, 213, 213));
                                    sp.jLabel2.setText(wordsPanel.jTable1.getValueAt(index, 0).toString());
                                    sp.jButton1.setText("Προσθήκη νεολογισμού στη βάση");
                                    add(sp);
                                    sp.setVisible(true);

                                    Word neologismos = new Word();
                                    neologismos.sentenceFinder(wordsPanel.jTable1.getValueAt(index, 0).toString());

                                    int start = 0;
                                    StyledDocument doc = null;

                                    for (String sentence : Sentences) {
                                        originalIndex = index;
                                        try {
                                            doc = sp.jTextPane1.getStyledDocument();
                                            SimpleAttributeSet keyWord = new SimpleAttributeSet();
                                            StyleConstants.setForeground(keyWord, Color.BLUE);
                                            StyleConstants.setBold(keyWord, true);
                                            keyWord.addAttribute("sentence", sentenceIndex);
                                            keyWord.addAttribute("index", originalIndex);
                                            doc.insertString(doc.getLength(), "\n\n", null);
                                            doc.insertString(doc.getLength(), sentence + "\n\n", null);

                                            String document = doc.getText(start, doc.getLength() - start);
                                            String nonAccentedSentence = XmlReader.stripAccents(document);
                                            String nonAccented = XmlReader.stripAccents(wordsPanel.jTable1.getValueAt(index, 0).toString());
                                            int startposition = nonAccentedSentence.toLowerCase(new java.util.Locale("el")).indexOf(nonAccented.toLowerCase(new java.util.Locale("el")));

                                            while (startposition >= 0) {
                                                int endposition = startposition + wordsPanel.jTable1.getValueAt(index, 0).toString().length();
                                                doc.remove(start + startposition, endposition - startposition);
                                                doc.insertString(start + startposition, RawWord.get(wordCounter), keyWord);
                                                String exactMatchSentence = document.substring(startposition, document.length());
                                                String nonAccentedSentence1 = XmlReader.stripAccents(exactMatchSentence);
                                                startposition = nonAccentedSentence.toLowerCase(new java.util.Locale("el")).indexOf(nonAccented.toLowerCase(new java.util.Locale("el")), startposition + 1);
                                                wordCounter++;
                                            }
                                            start = doc.getLength();
                                            sentenceIndex++;

                                        } catch (BadLocationException ex) {
                                            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                        counter++;
                                    }
                                    SimpleAttributeSet keyWord = new SimpleAttributeSet();
                                    StyleConstants.setBold(keyWord, true);
                                    doc.insertString(doc.getLength(), "Συνολικά: " + counter + " εμφάνιση/σεις " + "\n\n", keyWord);
                                } catch (BadLocationException ex) {
                                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                                }

                            }
                        }
                    });
                    return null;
                }

                @Override

                protected void done() {
                    jLabel4.setVisible(false);
                }
            };
            worker.execute();
        } else if (panel == 3) {
            SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() {
                    list = 2;
                    XmlReader reader = new XmlReader();
                    reader.parseXmlEnglish();
                    DictionariesController dic = new DictionariesController();
                    try {
                        dic.DatabaseConnector();
                    } catch (IOException ex) {
                        Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    dic.NewWordFinder();
                    Collator coll = Collator.getInstance(Locale.ENGLISH);
                    coll.setStrength(Collator.PRIMARY);
                    Collections.sort(neologismoi, coll);
                    Collections.sort(original, coll);

                    Set<String> StemSet = new LinkedHashSet<>();

                    int result;
                    for (String test : neologismoi) {
                        test = test.toLowerCase(Locale.ENGLISH);
                        EnglishStemmer es = new EnglishStemmer();
                        es.add(test.toCharArray(), test.length());
                        es.stem();
                        if (StemSet.add(es.toString().toUpperCase(Locale.ENGLISH))) {
                            stems.add(es.toString().toUpperCase(Locale.ENGLISH));
                        }
                    }

                    Set<Integer> SimilarSet = new LinkedHashSet<>();
                    boolean isSimilar = false;

                    for (int i = 0; i < neologismoi.size(); i++) {
                        if (!SimilarSet.isEmpty()) {
                            for (Integer index : SimilarSet) {
                                if (index == i) {
                                    isSimilar = true;
                                }
                            }
                        }

                        if (isSimilar == true) {
                            isSimilar = false;
                            continue;
                        }

                        SimilarWord similar = new SimilarWord();
                        similar.similarWords = neologismoi.get(i);

                        String test = neologismoi.get(i);
                        test = test.toLowerCase(Locale.ENGLISH);
                        EnglishStemmer es = new EnglishStemmer();
                        es.add(test.toCharArray(), test.length());
                        es.stem();

                        for (int j = 0; j < neologismoi.size(); j++) {
                            if (neologismoi.get(i) == null ? neologismoi.get(j) == null : neologismoi.get(i).equals(neologismoi.get(j))) {
                                continue;
                            }

                            String temp1;
                            temp1 = neologismoi.get(j).toLowerCase(Locale.ENGLISH);
                            EnglishStemmer es1 = new EnglishStemmer();
                            es1.add(temp1.toCharArray(), temp1.length());
                            es1.stem();

                            if (es.toString().equals(es1.toString())) {
                                similar.similarWords = similar.similarWords + " " + neologismoi.get(j);
                                SimilarSet.add(j);
                            }
                        }
                        alternatives.add(similar);
                    }

                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    dtm.setRowCount(stems.size());
                    jTable1.setModel(dtm);

                    DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
                    centerRenderer.setHorizontalAlignment(JLabel.CENTER);
                    jTable1.setDefaultRenderer(String.class, centerRenderer);

                    int listIndex;

                    listIndex = 0;
                    for (String stem : stems) {
                        jTable1.setValueAt(stem, listIndex, 0);
                        listIndex++;
                    }

                    listIndex = 0;
                    for (SimilarWord words : alternatives) {
                        jTable1.setValueAt(words.getSimilarWords(), listIndex, 1);
                        listIndex++;
                    }

                    TableRowSorter<TableModel> sorter = new TableRowSorter<>(jTable1.getModel());
                    jTable1.setRowSorter(sorter);
                    List<RowSorter.SortKey> sortKeys = new ArrayList<>();
                    sorter.setSortKeys(sortKeys);

                    jButton1.setEnabled(true);
                    jButton2.setEnabled(true);
                    jButton3.setEnabled(true);
                    jButton4.setEnabled(true);
                    jButton5.setEnabled(true);
                    jButton6.setEnabled(true);
                    jButton7.setEnabled(true);
                    jButton9.setEnabled(true);
                    jTable1.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent evt) {
                            JTable table = (JTable) evt.getSource();
                            if (evt.getClickCount() == 2) {
                                Point p = evt.getPoint();
                                int index = table.rowAtPoint(p);
                                jTextArea1.setVisible(false);
                                jScrollPane3.setVisible(false);
                                jButton3.setVisible(false);
                                jButton4.setVisible(false);
                                jButton5.setVisible(false);
                                jButton7.setVisible(false);
                                jButton9.setVisible(false);
                                jButton1.setVisible(false);
                                jLabel4.setVisible(false);
                                jButton2.setVisible(false);
                                jTextArea1.setVisible(false);
                                jToolBar1.setVisible(false);
                                jScrollPane3.setVisible(false);
                                jScrollPane2.setVisible(false);
                                jTable1.setVisible(false);
                                wordsPanel.setSize(800, 800);
                                wordsPanel.setBackground(new java.awt.Color(219, 213, 213));

                                wordsPanel.jTable1.setEnabled(false);
                                wordsPanel.jTable1.getTableHeader().setReorderingAllowed(false);

                                wordsPanel.jTable1.getTableHeader().setFont(new Font("Tahoma", Font.PLAIN, 13));
                                wordsPanel.jTable1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
                                    @Override
                                    public void mouseMoved(java.awt.event.MouseEvent e) {
                                        int row = wordsPanel.jTable1.rowAtPoint(e.getPoint());
                                        int col = wordsPanel.jTable1.columnAtPoint(e.getPoint());
                                        String value = "";
                                        if (col != 4 && col != 5) {
                                            value = wordsPanel.jTable1.getValueAt(row, col).toString();
                                        }
                                        if (!value.equals(null) && !value.equals("")) {
                                            wordsPanel.jTable1.setToolTipText(value);
                                        }
                                    }
                                });

                                wordsPanel.jLabel2.setText(jTable1.getValueAt(index, 0).toString());
                                int listIndex = 0;
                                String allWords = jTable1.getValueAt(index, 1).toString();
                                String[] splitStr = allWords.split("\\s+");
                                DefaultTableModel dtm = (DefaultTableModel) wordsPanel.jTable1.getModel();
                                dtm.setRowCount(splitStr.length);
                                wordsPanel.jTable1.setModel(dtm);
                                for (int i = 0; i < splitStr.length; i++) {
                                    wordsPanel.jTable1.setValueAt(splitStr[i], i, 0);
                                }

                                int emfaniseis;
                                int articleNumber;
                                Set<String> set = new LinkedHashSet<>();

                                for (int i = 0; i < splitStr.length; i++) {
                                    emfaniseis = 0;
                                    articleNumber = 0;
                                    set.clear();
                                    for (Word test2 : Words) {
                                        String lima = test2.nonAccentedWord.toUpperCase(Locale.ENGLISH);
                                        if (splitStr[i].equals(lima)) {
                                            if (set.add(test2.title)) {
                                                articleNumber++;
                                            }
                                            emfaniseis++;
                                        }
                                    }
                                    appearances.add(emfaniseis);
                                    articleNumbers.add(articleNumber);
                                }

                                for (int i = 0; i < splitStr.length; i++) {
                                    wordsPanel.jTable1.setValueAt(appearances.get(i), i, 1);
                                }

                                for (int i = 0; i < splitStr.length; i++) {
                                    wordsPanel.jTable1.setValueAt(articleNumbers.get(i), i, 2);
                                }

                                appearances.clear();
                                articleNumbers.clear();

                                DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
                                centerRenderer.setHorizontalAlignment(JLabel.CENTER);
                                wordsPanel.jTable1.setDefaultRenderer(String.class, centerRenderer);

                                add(wordsPanel);
                                wordsPanel.setVisible(true);
                            }
                        }
                    });

                    wordsPanel.jTable1.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent evt) {
                            JTable table = (JTable) evt.getSource();
                            if (evt.getClickCount() == 2) {
                                try {
                                    Point p = evt.getPoint();
                                    int index = table.rowAtPoint(p);
                                    wordsPanel.setVisible(false);
                                    sp.setVisible(true);
                                    sp.setSize(800, 800);
                                    sp.setBackground(new java.awt.Color(219, 213, 213));
                                    sp.jLabel2.setText(wordsPanel.jTable1.getValueAt(index, 0).toString());
                                    sp.jButton1.setText("Προσθήκη ξενόγλωσσου στη βάση");
                                    add(sp);
                                    sp.setVisible(true);

                                    Word neologismos = new Word();
                                    neologismos.sentenceFinder(wordsPanel.jTable1.getValueAt(index, 0).toString());

                                    int start = 0;
                                    StyledDocument doc = null;

                                    for (String sentence : Sentences) {
                                        originalIndex = index;
                                        try {
                                            doc = sp.jTextPane1.getStyledDocument();
                                            SimpleAttributeSet keyWord = new SimpleAttributeSet();
                                            StyleConstants.setForeground(keyWord, Color.BLUE);
                                            StyleConstants.setBold(keyWord, true);
                                            keyWord.addAttribute("sentence", sentenceIndex);
                                            keyWord.addAttribute("index", originalIndex);
                                            doc.insertString(doc.getLength(), "\n\n", null);
                                            doc.insertString(doc.getLength(), sentence + "\n\n", null);

                                            String document = doc.getText(start, doc.getLength() - start);
                                            String nonAccentedSentence = XmlReader.stripAccents(document);
                                            String nonAccented = XmlReader.stripAccents(wordsPanel.jTable1.getValueAt(index, 0).toString());
                                            int startposition = nonAccentedSentence.toLowerCase(new java.util.Locale("el")).indexOf(nonAccented.toLowerCase(new java.util.Locale("el")));

                                            while (startposition >= 0) {
                                                int endposition = startposition + wordsPanel.jTable1.getValueAt(index, 0).toString().length();
                                                doc.remove(start + startposition, endposition - startposition);
                                                doc.insertString(start + startposition, RawWord.get(wordCounter), keyWord);
                                                String exactMatchSentence = document.substring(startposition, document.length());
                                                String nonAccentedSentence1 = XmlReader.stripAccents(exactMatchSentence);
                                                startposition = nonAccentedSentence.toLowerCase(new java.util.Locale("el")).indexOf(nonAccented.toLowerCase(new java.util.Locale("el")), startposition + 1);
                                                wordCounter++;
                                            }
                                            start = doc.getLength();
                                            sentenceIndex++;

                                        } catch (BadLocationException ex) {
                                            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                        counter++;
                                    }
                                    SimpleAttributeSet keyWord = new SimpleAttributeSet();
                                    StyleConstants.setBold(keyWord, true);
                                    doc.insertString(doc.getLength(), "Συνολικά: " + counter + " εμφάνιση/σεις " + "\n\n", keyWord);
                                } catch (BadLocationException ex) {
                                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                                }

                            }
                        }
                    });
                    return null;
                }

                @Override

                protected void done() {
                    jLabel4.setVisible(false);
                }
            };
            worker.execute();
        } else if (panel == 4) {
            SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() {
                    list = 3;
                    XmlReader reader = new XmlReader();
                    reader.parseXml();

                    DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
                    dtm.setRowCount(Xmls.size());
                    jTable1.setModel(dtm);

                    DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
                    centerRenderer.setHorizontalAlignment(JLabel.CENTER);
                    jTable1.setDefaultRenderer(String.class, centerRenderer);

                    int listIndex;

                    listIndex = 0;
                    for (String xml : Xmls) {
                        jTable1.setValueAt(xml, listIndex, 0);
                        listIndex++;
                    }

                    listIndex = 0;
                    for (String title : Titles) {
                        jTable1.setValueAt(title, listIndex, 1);
                        listIndex++;
                    }

                    jButton1.setEnabled(true);
                    jButton2.setEnabled(true);
                    jButton3.setEnabled(true);
                    jButton4.setEnabled(true);
                    jButton5.setEnabled(true);
                    jButton6.setEnabled(true);
                    jButton7.setEnabled(false);
                    jButton9.setEnabled(true);
                    jTable1.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent evt) {
                            JTable table = (JTable) evt.getSource();
                            if (evt.getClickCount() == 2) {
                                try {
                                    Point p = evt.getPoint();
                                    int index = table.rowAtPoint(p);
                                    jTextArea1.setVisible(false);
                                    jScrollPane3.setVisible(false);
                                    jButton3.setVisible(false);
                                    jButton4.setVisible(false);
                                    jButton5.setVisible(false);
                                    jButton7.setVisible(false);
                                    jButton9.setVisible(false);
                                    jButton1.setVisible(false);
                                    jLabel4.setVisible(false);
                                    jButton2.setVisible(false);
                                    jTextArea1.setVisible(false);
                                    jToolBar1.setVisible(false);
                                    jScrollPane3.setVisible(false);
                                    jScrollPane2.setVisible(false);
                                    jTable1.setVisible(false);
                                    keimeno1.setSize(800, 800);
                                    keimeno1.setBackground(new java.awt.Color(219, 213, 213));
                                    keimeno1.jTextPane1.setText("");
                                    keimeno1.jTextField1.setText("");
                                    keimeno1.jTextPane2.setText("");
                                    add(keimeno1);
                                    keimeno1.setVisible(true);
                                    keimeno1.jTextField1.setText(Xmls.get(index));
                                    keimeno1.jTextPane2.setText(Titles.get(index));
                                    StyledDocument input = keimeno1.jTextPane1.getStyledDocument();
                                    SimpleAttributeSet notBold = new SimpleAttributeSet();
                                    StyleConstants.setBold(notBold, false);
                                    input.insertString(input.getStartPosition().getOffset(), Keimena.get(index).getKeimeno(), notBold);

                                    if (!Keimena.get(index).isClassified()) {

                                        StyledDocument doc = keimeno1.jTextPane1.getStyledDocument();
                                        String document = doc.getText(doc.getStartPosition().getOffset(), doc.getEndPosition().getOffset() - doc.getStartPosition().getOffset());

                                        SimpleAttributeSet question = new SimpleAttributeSet();
                                        StyleConstants.setBold(question, true);

                                        SimpleAttributeSet answer1 = new SimpleAttributeSet();
                                        StyleConstants.setBold(answer1, true);
                                        answer1.addAttribute("answer", "YES");
                                        answer1.addAttribute("index", index);

                                        SimpleAttributeSet answer2 = new SimpleAttributeSet();
                                        StyleConstants.setBold(answer2, true);
                                        answer2.addAttribute("answer", "NO");
                                        answer2.addAttribute("index", index);

                                        SimpleAttributeSet answer = new SimpleAttributeSet();
                                        String classification = "Επιτυχής Κατηγοριοποίηση; (ΝΑΙ ΟΧΙ)";
                                        String classification1 = "Επιτυχής Κατηγοριοποίηση; (";
                                        String classification2 = ")";
                                        int position = document.indexOf(classification);
                                        if (position > 0) {
                                            int endposition = position + classification.length();
                                            doc.remove(position, endposition - position);
                                            doc.insertString(position, classification1, question);
                                            int position1 = position + classification1.length();
                                            doc.insertString(position1, "ΝΑΙ", answer1);
                                            doc.insertString(position1 + 3, " ", null);
                                            doc.insertString(position1 + 4, "ΟΧΙ", answer2);
                                            doc.insertString(position1 + 7, ")", question);
                                        }

                                        SimpleAttributeSet link = new SimpleAttributeSet();
                                        link.addAttribute("open", "just_do_it");

                                        String linkInText = "Σύνδεσμος στο άρθρο: ";
                                        int position1 = document.indexOf(linkInText);
                                        int end = position1 + linkInText.length();

                                        String nextMeta = "Ημερομ.";
                                        int position2 = document.indexOf(nextMeta);

                                        String le_link = doc.getText(end, position2 - end);
                                        link.addAttribute("actual_link", le_link);
                                        doc.remove(end, position2 - end);
                                        doc.insertString(end, le_link, link);
                                    }
                                } catch (BadLocationException ex) {
                                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                                }

                            }
                        }
                    });
                    return null;
                }

                @Override

                protected void done() {
                    jLabel4.setVisible(false);
                }
            };
            worker.execute();
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        panel = 1;
        repaint();

        if (list == 1) {
            int listIndex;
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            model.setRowCount(stems.size());

            listIndex = 0;
            for (String stem : stems) {
                jTable1.setValueAt(stem, listIndex, 0);
                listIndex++;
            }

            listIndex = 0;
            for (SimilarWord alternative : alternatives) {
                jTable1.setValueAt(alternative.similarWords, listIndex, 1);
                listIndex++;
            }

            jButton7.setEnabled(true);

        }
        if (list == 2) {
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            model.setRowCount(0);
            jButton7.setEnabled(false);
        }

        if (list == 3) {
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            model.setRowCount(0);
            jButton7.setEnabled(false);
        }

        jButton4.setBackground(new java.awt.Color(255, 255, 255));
        jButton3.setBackground(new java.awt.Color(255, 153, 102));
        jButton5.setBackground(new java.awt.Color(255, 153, 102));
        jButton9.setBackground(new java.awt.Color(255, 153, 102));
        jTextArea1.setVisible(true);
        jScrollPane3.setVisible(true);
        jButton1.setVisible(true);
        jButton2.setVisible(true);
        jButton6.setVisible(true);
        jButton7.setVisible(true);
        jTextArea1.setVisible(true);
        jScrollPane2.setVisible(true);
        jTable1.getColumnModel().getColumn(0).setHeaderValue("Βάση (μέσω stemming)");
        jTable1.getColumnModel().getColumn(1).setHeaderValue("Τύποι");
        jTable1.setVisible(true);
        leksiko.setVisible(false);
        jButton2.setFont(new Font("Tahoma", Font.PLAIN, 13));
        jButton2.setText("Αναζήτηση Νεολογισμών");
        jButton2.setToolTipText("Ξεκινήστε την αναζήτηση νεολογισμών");
        jTable1.setToolTipText("Η λίστα με τους υποψήφιους νεολογισμούς");
        getContentPane().validate();
        getContentPane().revalidate();
        getContentPane().repaint();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        panel = 3;
        repaint();

        if (list == 1) {
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            model.setRowCount(0);
            jButton7.setEnabled(false);
        }
        if (list == 2) {
            int listIndex;
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            model.setRowCount(stems.size());
            listIndex = 0;
            for (String stem : stems) {
                jTable1.setValueAt(stem, listIndex, 0);
                listIndex++;
            }

            listIndex = 0;
            for (SimilarWord alternative : alternatives) {
                jTable1.setValueAt(alternative.similarWords, listIndex, 1);
                listIndex++;
            }
            jButton7.setEnabled(true);
        }

        if (list == 3) {
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            model.setRowCount(0);
            jButton7.setEnabled(false);
        }

        jButton3.setBackground(new java.awt.Color(255, 153, 102));
        jButton4.setBackground(new java.awt.Color(255, 153, 102));
        jButton5.setBackground(new java.awt.Color(255, 255, 255));
        jButton9.setBackground(new java.awt.Color(255, 153, 102));
        jButton6.setVisible(true);
        jButton1.setVisible(true);
        jButton2.setVisible(true);
        jButton7.setVisible(true);
        jScrollPane3.setVisible(true);
        jTextArea1.setVisible(true);
        jScrollPane2.setVisible(true);
        jTable1.getColumnModel().getColumn(0).setHeaderValue("Βάση (μέσω stemming)");
        jTable1.getColumnModel().getColumn(1).setHeaderValue("Τύποι");
        jTable1.setVisible(true);
        leksiko.setVisible(false);
        jButton2.setFont(new Font("Tahoma", Font.PLAIN, 13));
        jButton2.setText("Αναζήτηση Ξενόγλωσσων");
        jButton2.setToolTipText("Ξεκινήστε την αναζήτηση ξενόγλωσσων");
        jTable1.setToolTipText("Η λίστα με τους υποψήφιους ξενόγλωσσους");
        getContentPane().validate();
        getContentPane().revalidate();
        getContentPane().repaint();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        panel = 2;
        jButton4.setBackground(new java.awt.Color(255, 153, 102));
        jButton3.setBackground(new java.awt.Color(255, 255, 255));
        jButton5.setBackground(new java.awt.Color(255, 153, 102));
        jButton9.setBackground(new java.awt.Color(255, 153, 102));
        jTextArea1.setVisible(false);
        jScrollPane3.setVisible(false);
        jButton1.setVisible(false);
        jButton2.setVisible(false);
        jButton7.setVisible(false);
        jScrollPane2.setVisible(false);
        jTable1.setVisible(false);
        leksiko.setSize(800, 800);
        leksiko.setBackground(new java.awt.Color(219, 213, 213));
        add(leksiko);
        leksiko.setVisible(true);
        getContentPane().validate();
        getContentPane().revalidate();
        getContentPane().repaint();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        try {
            if (panel == 1) {
                ListExporter.exportList(jTextArea1);
            } else {
                ListExporter.exportListEnglish(jTextArea1);
            }
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        JOptionPane.showMessageDialog(this, "Το ευρετήριο δημιουργήθηκε", "", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        panel = 4;
        repaint();

        if (list == 1) {
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            model.setRowCount(0);
            jButton7.setEnabled(false);
        }
        if (list == 2) {
            DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
            model.setRowCount(0);
            jButton7.setEnabled(false);
        }

        if (list == 3) {
            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            dtm.setRowCount(Xmls.size());
            jTable1.setModel(dtm);

            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment(JLabel.CENTER);
            jTable1.setDefaultRenderer(String.class, centerRenderer);

            int listIndex;

            listIndex = 0;
            for (String xml : Xmls) {
                jTable1.setValueAt(xml, listIndex, 0);
                listIndex++;
            }

            listIndex = 0;
            for (String title : Titles) {
                jTable1.setValueAt(title, listIndex, 1);
                listIndex++;
            }
        }

        jButton4.setBackground(new java.awt.Color(255, 153, 102));
        jButton3.setBackground(new java.awt.Color(255, 153, 102));
        jButton5.setBackground(new java.awt.Color(255, 153, 102));
        jButton9.setBackground(new java.awt.Color(255, 255, 255));
        jTextArea1.setVisible(true);
        jScrollPane3.setVisible(true);
        jButton1.setVisible(true);
        jButton2.setFont(new Font("Tahoma", Font.PLAIN, 13));
        jButton2.setText("Κατηγοριοποίηση Άρθρων");
        jButton2.setToolTipText("Ξεκινήστε την εισαγωγή άρθρων");
        jTable1.setToolTipText("Η λίστα με τα εισαχθέντα άρθρα");
        jTable1.getColumnModel().getColumn(0).setHeaderValue("Όνομα Αρχείου XML");
        jTable1.getColumnModel().getColumn(1).setHeaderValue("Τίτλος Άρθρου");
        jButton2.setVisible(true);
        jButton7.setVisible(false);
        jScrollPane2.setVisible(true);
        jTable1.setVisible(true);
        leksiko.setVisible(false);
        getContentPane().validate();
        getContentPane().revalidate();
        getContentPane().repaint();
    }//GEN-LAST:event_jButton9ActionPerformed

    class event7 extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent evt) {
            SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() {
                    Point pt = evt.getPoint();
                    if (evt.getClickCount() == 1) {
                        int location = keimeno.jTextPane1.viewToModel(pt);
                        StyledDocument doc = keimeno.jTextPane1.getStyledDocument();
                        Element el = doc.getCharacterElement(location);
                        AttributeSet set = el.getAttributes();
                        if (set.containsAttribute("answer", "YES")) {
                            if (active == false) {
                                try {
                                    String document = doc.getText(doc.getStartPosition().getOffset(), doc.getEndPosition().getOffset() - doc.getStartPosition().getOffset());
                                    String classification = "Επιτυχής Κατηγοριοποίηση; (ΝΑΙ ΟΧΙ)";
                                    int position = document.indexOf(classification);
                                    int endposition = position + classification.length();
                                    doc.remove(position, endposition - position);
                                    int index = Integer.parseInt(set.getAttribute("index").toString());
                                    String oldText = Words.get(index).getRawKeimeno();
                                    String newText = doc.getText(doc.getStartPosition().getOffset(), doc.getEndPosition().getOffset() - doc.getStartPosition().getOffset());

                                    for (int i = 0; i < Words.size(); i++) {
                                        if (Words.get(index).getTitle() != null && Words.get(i).getTitle() != null && Words.get(index).getTitle().equals(Words.get(i).getTitle())) {
                                            Words.get(i).setKeimeno(newText);
                                            Words.get(i).setClassified(true);
                                        }
                                    }
                                    Words.get(index).setClassified(true);
                                    Words.get(index).setKeimeno(newText);
                                    XmlWriter writer = new XmlWriter(new File(Words.get(index).getRawXml()));
                                    int index1 = newText.indexOf("Θεματική: ");
                                    String subject = newText.substring(index1 + 10, newText.length());
                                    ArrayList<String> cat = new ArrayList();
                                    cat.add(subject);
                                    writer.updateXml(Words.get(index).getLink(), cat);

                                    oldText = oldText.replaceAll("[%$#@!^&*\\[\\]!?,<>\".()«»]:“”", "").replaceAll("•", "").replaceAll("\\s{2,}", " ").replaceAll("(\\d+)(:)(\\d+)(,+)", "").replaceAll("(\\d+)(:)(\\d+)(.)(,+)", "").replaceAll("(\\/)(.{10,})(\\/)", "").replaceAll(" \\. ", " ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("΄", " ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("΄", " ").replaceAll("'", "").replaceAll("\"", "");
                                    String[] words = oldText.split("\\s+");
                                    String stemmedKeimeno = "";
                                    int latin_count = 0;
                                    boolean latin_flag = false;
                                    for (String word : words) {
                                        if (stopwords.contains(word) || word.length() <= 2) {
                                            continue;
                                        }
                                        Stemmer stemmer = new Stemmer();
                                        int result = stemmer.stem(word.toCharArray(), word.length());
                                        word = word.substring(0, result);
                                        if (word.substring(0, 1).matches("[a-zA-Z]")) {
                                            latin_count++;
                                            if (latin_count > 30) {
                                                stemmedKeimeno = "";
                                                latin_flag = true;
                                                break;
                                            }
                                        }
                                        stemmedKeimeno = stemmedKeimeno + word + "\t";
                                    }

                                    if (latin_flag) {
                                        for (String word : words) {
                                            if (stopwords.contains(word) || word.length() <= 2) {
                                                continue;
                                            }
                                            EnglishStemmer stemmer = new EnglishStemmer();
                                            stemmer.add(word.toCharArray(), word.length());
                                            stemmer.stem();
                                            word = stemmer.toString();
                                            stemmedKeimeno = stemmedKeimeno + word + "\t";
                                        }
                                    }
                                    int[] selections = new int[17];

                                    for (int i = 0; i < selections.length; i++) {
                                        selections[i] = 0;
                                    }

                                    String[] cats = subject.split(",");
                                    for (String s : cats) {
                                        for (int i = 0; i < classificationCategories.size(); i++) {
                                            if (s.contains(classificationCategories.get(i))) {
                                                selections[i] = 1;
                                            }
                                        }
                                    }

                                    try (
                                            FileWriter fw = new FileWriter(System.getProperty("user.dir") + "/files/classification/level1/meka_training.arff", true);
                                            BufferedWriter bw = new BufferedWriter(fw);
                                            PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File(System.getProperty("user.dir") + "/files/classification/level1/meka_training.arff"), true), "UTF-8"))) {
                                        //PrintWriter out = new PrintWriter(System.getProperty("user.dir") + "/files/classification/level1/meka_training.arff", "UTF-8")) {
                                        for (int i = 0; i < selections.length; i++) {
                                            out.print(selections[i] + ",");
                                        }
                                        out.println("\'" + stemmedKeimeno + "\'" + "\t");
                                    } catch (IOException e) {
                                    }
                                } catch (BadLocationException ex) {
                                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                active = false;
                            }
                        } else if (set.containsAttribute("answer", "NO")) {
                            if (active == false) {
                                Categories cat = new Categories();
                                cat.setVisible(true);
                                cat.setLocationRelativeTo(null);
                                active = true;
                                cat.jButton1.addMouseListener(new MouseAdapter() {
                                    public void mouseClicked(MouseEvent evt) {
                                        active = false;
                                        ArrayList<String> cats = new ArrayList();
                                        int[] selections = new int[17];
                                        for (int i = 0; i < selections.length; i++) {
                                            selections[i] = 0;
                                        }
                                        if (cat.jToggleButton1.isSelected()) {
                                            cats.add(cat.jToggleButton1.getText());
                                            selections[0] = 1;
                                        }
                                        if (cat.jToggleButton2.isSelected()) {
                                            cats.add(cat.jToggleButton2.getText());
                                            selections[2] = 1;
                                        }
                                        if (cat.jToggleButton3.isSelected()) {
                                            cats.add(cat.jToggleButton3.getText());
                                            selections[1] = 1;
                                        }
                                        if (cat.jToggleButton4.isSelected()) {
                                            cats.add(cat.jToggleButton4.getText());
                                            selections[3] = 1;
                                        }
                                        if (cat.jToggleButton5.isSelected()) {
                                            cats.add(cat.jToggleButton5.getText());
                                            selections[5] = 1;
                                        }
                                        if (cat.jToggleButton6.isSelected()) {
                                            cats.add(cat.jToggleButton6.getText());
                                            selections[7] = 1;
                                        }
                                        if (cat.jToggleButton7.isSelected()) {
                                            cats.add(cat.jToggleButton7.getText());
                                            selections[8] = 1;
                                        }
                                        if (cat.jToggleButton8.isSelected()) {
                                            cats.add(cat.jToggleButton8.getText());
                                            selections[9] = 1;
                                        }
                                        if (cat.jToggleButton9.isSelected()) {
                                            cats.add(cat.jToggleButton9.getText());
                                            selections[14] = 1;
                                        }
                                        if (cat.jToggleButton10.isSelected()) {
                                            cats.add(cat.jToggleButton10.getText());
                                            selections[10] = 1;
                                        }
                                        if (cat.jToggleButton11.isSelected()) {
                                            cats.add(cat.jToggleButton11.getText());
                                            selections[11] = 1;
                                        }
                                        if (cat.jToggleButton12.isSelected()) {
                                            cats.add(cat.jToggleButton12.getText());
                                            selections[15] = 1;
                                        }
                                        if (cat.jToggleButton13.isSelected()) {
                                            cats.add(cat.jToggleButton13.getText());
                                            selections[16] = 1;
                                        }
                                        if (cat.jToggleButton14.isSelected()) {
                                            cats.add(cat.jToggleButton14.getText());
                                            selections[4] = 1;
                                        }
                                        if (cat.jToggleButton15.isSelected()) {
                                            cats.add(cat.jToggleButton15.getText());
                                            selections[6] = 1;
                                        }
                                        if (cat.jToggleButton16.isSelected()) {
                                            cats.add(cat.jToggleButton16.getText());
                                            selections[12] = 1;
                                        }
                                        if (cat.jToggleButton17.isSelected()) {
                                            cats.add(cat.jToggleButton17.getText());
                                            selections[13] = 1;
                                        }
                                        cat.dispose();
                                        try {
                                            String document = doc.getText(doc.getStartPosition().getOffset(), doc.getEndPosition().getOffset() - doc.getStartPosition().getOffset());
                                            String classification = "Επιτυχής Κατηγοριοποίηση; (ΝΑΙ ΟΧΙ)";
                                            int position = document.indexOf(classification);
                                            int endposition = position + classification.length();
                                            doc.remove(position, endposition - position);
                                            String tmp = "Θεματική: ";
                                            String document1 = doc.getText(doc.getStartPosition().getOffset(), doc.getEndPosition().getOffset() - doc.getStartPosition().getOffset());
                                            int position1 = document1.indexOf(tmp);
                                            int endposition1 = position1 + tmp.length();
                                            doc.remove(endposition1, doc.getEndPosition().getOffset() - endposition1 - 1);

                                            for (int i = 0; i < cats.size(); i++) {
                                                doc.insertString(doc.getEndPosition().getOffset(), cats.get(i), null);
                                                if (i != cats.size() - 1) {
                                                    doc.insertString(doc.getEndPosition().getOffset(), ", ", null);
                                                } else {
                                                    doc.insertString(doc.getEndPosition().getOffset(), "\n", null);
                                                }

                                            }
                                            int index = Integer.parseInt(set.getAttribute("index").toString());
                                            String oldText = Words.get(index).getRawKeimeno();
                                            String newText = doc.getText(doc.getStartPosition().getOffset(), doc.getEndPosition().getOffset() - doc.getStartPosition().getOffset());

                                            for (int i = 0; i < Words.size(); i++) {
                                                if (Words.get(index).getTitle() != null && Words.get(i).getTitle() != null && Words.get(index).getTitle().equals(Words.get(i).getTitle())) {
                                                    Words.get(i).setKeimeno(newText);
                                                    Words.get(i).setClassified(true);
                                                }
                                            }
                                            Words.get(index).setClassified(true);
                                            Words.get(index).setKeimeno(newText);
                                            String link = Words.get(index).getLink();
                                            File file = new File(Words.get(index).getRawXml());
                                            XmlWriter writer = new XmlWriter(file);
                                            writer.updateXml(link, cats);

                                            oldText = oldText.replaceAll("[%$#@!^&*\\[\\]!?,<>\".()«»]:“”", "").replaceAll("•", "").replaceAll("\\s{2,}", " ").replaceAll("(\\d+)(:)(\\d+)(,+)", "").replaceAll("(\\d+)(:)(\\d+)(.)(,+)", "").replaceAll("(\\/)(.{10,})(\\/)", "").replaceAll(" \\. ", " ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("΄", " ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("΄", " ").replaceAll("'", "").replaceAll("\"", "");
                                            String[] words = oldText.split("\\s+");
                                            String stemmedKeimeno = "";
                                            int latin_count = 0;
                                            boolean latin_flag = false;
                                            for (String word : words) {
                                                if (stopwords.contains(word) || word.length() <= 2) {
                                                    continue;
                                                }
                                                Stemmer stemmer = new Stemmer();
                                                int result = stemmer.stem(word.toCharArray(), word.length());
                                                word = word.substring(0, result);
                                                if (word.substring(0, 1).matches("[a-zA-Z]")) {
                                                    latin_count++;
                                                    if (latin_count > 30) {
                                                        stemmedKeimeno = "";
                                                        latin_flag = true;
                                                        break;
                                                    }
                                                }
                                                stemmedKeimeno = stemmedKeimeno + word + "\t";
                                            }

                                            if (latin_flag) {
                                                for (String word : words) {
                                                    if (stopwords.contains(word) || word.length() <= 2) {
                                                        continue;
                                                    }
                                                    EnglishStemmer stemmer = new EnglishStemmer();
                                                    stemmer.add(word.toCharArray(), word.length());
                                                    stemmer.stem();
                                                    word = stemmer.toString();
                                                    stemmedKeimeno = stemmedKeimeno + word + "\t";
                                                }
                                            }

                                            try (
                                                    //FileWriter fw = new FileWriter(System.getProperty("user.dir") + "/files/classification/level1/meka_training.arff", true);
                                                    //BufferedWriter bw = new BufferedWriter(fw);
                                                    //PrintWriter out = new PrintWriter(bw);
                                                    PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File(System.getProperty("user.dir") + "/files/classification/level1/meka_training.arff"), true), "UTF-8"))) {
                                                for (int i = 0; i < selections.length; i++) {
                                                    out.print(selections[i] + ",");
                                                }
                                                out.println("\'" + stemmedKeimeno + "\'" + "\t");
                                            } catch (IOException e) {
                                            }
                                        } catch (BadLocationException ex) {
                                            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                });
                                cat.jButton2.addMouseListener(new MouseAdapter() {
                                    public void mouseClicked(MouseEvent evt) {
                                        cat.dispose();
                                        active = false;
                                    }
                                });
                                cat.addWindowListener(new WindowAdapter() {
                                    @Override
                                    public void windowClosing(WindowEvent we) {
                                        active = false;
                                    }
                                });

                            }
                        }
                    }
                    return null;
                }

                @Override

                protected void done() {
                }
            };
            worker.execute();
        }
    }

    class event10 extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent evt) {
            SwingWorker<Void, Void> worker;
            worker = new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() {
                    Point pt = evt.getPoint();
                    if (evt.getClickCount() == 1) {
                        int location = keimeno1.jTextPane1.viewToModel(pt);
                        StyledDocument doc = keimeno1.jTextPane1.getStyledDocument();
                        Element el = doc.getCharacterElement(location);
                        AttributeSet set = el.getAttributes();
                        if (set.containsAttribute("answer", "YES")) {
                            if (active == false) {
                                try {
                                    String document = doc.getText(doc.getStartPosition().getOffset(), doc.getEndPosition().getOffset() - doc.getStartPosition().getOffset());
                                    String classification = "Επιτυχής Κατηγοριοποίηση; (ΝΑΙ ΟΧΙ)";
                                    int position = document.indexOf(classification);
                                    int endposition = position + classification.length();
                                    doc.remove(position, endposition - position);
                                    int index = Integer.parseInt(set.getAttribute("index").toString());
                                    String oldText = Keimena.get(index).getRawKeimeno();
                                    String newText = doc.getText(doc.getStartPosition().getOffset(), doc.getEndPosition().getOffset() - doc.getStartPosition().getOffset());

                                    Keimena.get(index).setClassified(true);
                                    Keimena.get(index).setKeimeno(newText);

                                    XmlWriter writer = new XmlWriter(new File(Keimena.get(index).getRawXml()));
                                    int index1 = newText.indexOf("Θεματική: ");
                                    String subject = newText.substring(index1 + 10, newText.length());
                                    ArrayList<String> cat = new ArrayList();
                                    cat.add(subject);
                                    writer.updateXml(Keimena.get(index).getLink(), cat);

                                    oldText = oldText.replaceAll("[%$#@!^&*\\[\\]!?,<>\".()«»]:“”", "").replaceAll("•", "").replaceAll("\\s{2,}", " ").replaceAll("(\\d+)(:)(\\d+)(,+)", "").replaceAll("(\\d+)(:)(\\d+)(.)(,+)", "").replaceAll("(\\/)(.{10,})(\\/)", "").replaceAll(" \\. ", " ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("΄", " ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("΄", " ").replaceAll("'", "").replaceAll("\"", "");
                                    String[] words = oldText.split("\\s+");
                                    String stemmedKeimeno = "";
                                    int latin_count = 0;
                                    boolean latin_flag = false;
                                    for (String word : words) {
                                        if (stopwords.contains(word) || word.length() <= 2) {
                                            continue;
                                        }
                                        Stemmer stemmer = new Stemmer();
                                        int result = stemmer.stem(word.toCharArray(), word.length());
                                        word = word.substring(0, result);
                                        if (word.substring(0, 1).matches("[a-zA-Z]")) {
                                            latin_count++;
                                            if (latin_count > 30) {
                                                stemmedKeimeno = "";
                                                latin_flag = true;
                                                break;
                                            }
                                        }
                                        stemmedKeimeno = stemmedKeimeno + word + "\t";
                                    }

                                    if (latin_flag) {
                                        for (String word : words) {
                                            if (stopwords.contains(word) || word.length() <= 2) {
                                                continue;
                                            }
                                            EnglishStemmer stemmer = new EnglishStemmer();
                                            stemmer.add(word.toCharArray(), word.length());
                                            stemmer.stem();
                                            word = stemmer.toString();
                                            stemmedKeimeno = stemmedKeimeno + word + "\t";
                                        }
                                    }

                                    int[] selections = new int[17];

                                    for (int i = 0; i < selections.length; i++) {
                                        selections[i] = 0;
                                    }

                                    String[] cats = subject.split(",");
                                    for (String s : cats) {
                                        for (int i = 0; i < classificationCategories.size(); i++) {
                                            if (s.contains(classificationCategories.get(i))) {
                                                selections[i] = 1;
                                            }
                                        }
                                    }

                                    try (
                                            FileWriter fw = new FileWriter(System.getProperty("user.dir") + "/files/classification/level1/meka_training.arff", true);
                                            BufferedWriter bw = new BufferedWriter(fw);
                                            PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File(System.getProperty("user.dir") + "/files/classification/level1/meka_training.arff"), true), "UTF-8"))) {
                                        //PrintWriter out = new PrintWriter(System.getProperty("user.dir") + "/files/classification/level1/meka_training.arff", "UTF-8")) {
                                        for (int i = 0; i < selections.length; i++) {
                                            out.print(selections[i] + ",");
                                        }
                                        out.println("\'" + stemmedKeimeno + "\'" + "\t");
                                    } catch (IOException e) {
                                    }
                                } catch (BadLocationException ex) {
                                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                active = false;
                            }
                        } else if (set.containsAttribute("answer", "NO")) {
                            if (active == false) {
                                Categories cat = new Categories();
                                cat.setVisible(true);
                                cat.setLocationRelativeTo(null);
                                active = true;
                                cat.jButton1.addMouseListener(new MouseAdapter() {
                                    public void mouseClicked(MouseEvent evt) {
                                        active = false;
                                        ArrayList<String> cats = new ArrayList();
                                        int[] selections = new int[17];
                                        for (int i = 0; i < selections.length; i++) {
                                            selections[i] = 0;
                                        }
                                        if (cat.jToggleButton1.isSelected()) {
                                            cats.add(cat.jToggleButton1.getText());
                                            selections[0] = 1;
                                        }
                                        if (cat.jToggleButton2.isSelected()) {
                                            cats.add(cat.jToggleButton2.getText());
                                            selections[2] = 1;
                                        }
                                        if (cat.jToggleButton3.isSelected()) {
                                            cats.add(cat.jToggleButton3.getText());
                                            selections[1] = 1;
                                        }
                                        if (cat.jToggleButton4.isSelected()) {
                                            cats.add(cat.jToggleButton4.getText());
                                            selections[3] = 1;
                                        }
                                        if (cat.jToggleButton5.isSelected()) {
                                            cats.add(cat.jToggleButton5.getText());
                                            selections[5] = 1;
                                        }
                                        if (cat.jToggleButton6.isSelected()) {
                                            cats.add(cat.jToggleButton6.getText());
                                            selections[7] = 1;
                                        }
                                        if (cat.jToggleButton7.isSelected()) {
                                            cats.add(cat.jToggleButton7.getText());
                                            selections[8] = 1;
                                        }
                                        if (cat.jToggleButton8.isSelected()) {
                                            cats.add(cat.jToggleButton8.getText());
                                            selections[9] = 1;
                                        }
                                        if (cat.jToggleButton9.isSelected()) {
                                            cats.add(cat.jToggleButton9.getText());
                                            selections[14] = 1;
                                        }
                                        if (cat.jToggleButton10.isSelected()) {
                                            cats.add(cat.jToggleButton10.getText());
                                            selections[10] = 1;
                                        }
                                        if (cat.jToggleButton11.isSelected()) {
                                            cats.add(cat.jToggleButton11.getText());
                                            selections[11] = 1;
                                        }
                                        if (cat.jToggleButton12.isSelected()) {
                                            cats.add(cat.jToggleButton12.getText());
                                            selections[15] = 1;
                                        }
                                        if (cat.jToggleButton13.isSelected()) {
                                            cats.add(cat.jToggleButton13.getText());
                                            selections[16] = 1;
                                        }
                                        if (cat.jToggleButton14.isSelected()) {
                                            cats.add(cat.jToggleButton14.getText());
                                            selections[4] = 1;
                                        }
                                        if (cat.jToggleButton15.isSelected()) {
                                            cats.add(cat.jToggleButton15.getText());
                                            selections[6] = 1;
                                        }
                                        if (cat.jToggleButton16.isSelected()) {
                                            cats.add(cat.jToggleButton16.getText());
                                            selections[12] = 1;
                                        }
                                        if (cat.jToggleButton17.isSelected()) {
                                            cats.add(cat.jToggleButton17.getText());
                                            selections[13] = 1;
                                        }
                                        cat.dispose();
                                        try {
                                            String document = doc.getText(doc.getStartPosition().getOffset(), doc.getEndPosition().getOffset() - doc.getStartPosition().getOffset());
                                            String classification = "Επιτυχής Κατηγοριοποίηση; (ΝΑΙ ΟΧΙ)";
                                            int position = document.indexOf(classification);
                                            int endposition = position + classification.length();
                                            doc.remove(position, endposition - position);
                                            String tmp = "Θεματική: ";
                                            String document1 = doc.getText(doc.getStartPosition().getOffset(), doc.getEndPosition().getOffset() - doc.getStartPosition().getOffset());
                                            int position1 = document1.indexOf(tmp);
                                            int endposition1 = position1 + tmp.length();
                                            doc.remove(endposition1, doc.getEndPosition().getOffset() - endposition1 - 1);

                                            for (int i = 0; i < cats.size(); i++) {
                                                doc.insertString(doc.getEndPosition().getOffset(), cats.get(i), null);
                                                if (i != cats.size() - 1) {
                                                    doc.insertString(doc.getEndPosition().getOffset(), ", ", null);
                                                } else {
                                                    doc.insertString(doc.getEndPosition().getOffset(), "\n", null);
                                                }

                                            }
                                            int index = Integer.parseInt(set.getAttribute("index").toString());
                                            String oldText = Keimena.get(index).getRawKeimeno();
                                            String newText = doc.getText(doc.getStartPosition().getOffset(), doc.getEndPosition().getOffset() - doc.getStartPosition().getOffset());

                                            Keimena.get(index).setClassified(true);
                                            Keimena.get(index).setKeimeno(newText);

                                            String link = Keimena.get(index).getLink();
                                            File file = new File(Keimena.get(index).getRawXml());
                                            XmlWriter writer = new XmlWriter(file);
                                            writer.updateXml(link, cats);

                                            oldText = oldText.replaceAll("[%$#@!^&*\\[\\]!?,<>\".()«»]:“”", "").replaceAll("•", "").replaceAll("\\s{2,}", " ").replaceAll("(\\d+)(:)(\\d+)(,+)", "").replaceAll("(\\d+)(:)(\\d+)(.)(,+)", "").replaceAll("(\\/)(.{10,})(\\/)", "").replaceAll(" \\. ", " ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("΄", " ").replaceAll("A", "Α").replaceAll("B", "Β").replaceAll("E", "Ε").replaceAll("H", "Η").replaceAll("I", "Ι").replaceAll("K", "Κ").replaceAll("M", "Μ").replaceAll("N", "Ν").replaceAll("O", "Ο").replaceAll("P", "Ρ").replaceAll("T", "Τ").replaceAll("Y", "Υ").replaceAll("X", "Χ").replaceAll("Z", "Ζ").replaceAll("µ", "μ").replaceAll("΄", " ").replaceAll("'", "").replaceAll("\"", "");
                                            String[] words = oldText.split("\\s+");
                                            String stemmedKeimeno = "";
                                            int latin_count = 0;
                                            boolean latin_flag = false;
                                            for (String word : words) {
                                                if (stopwords.contains(word) || word.length() <= 2) {
                                                    continue;
                                                }
                                                Stemmer stemmer = new Stemmer();
                                                int result = stemmer.stem(word.toCharArray(), word.length());
                                                word = word.substring(0, result);
                                                if (word.substring(0, 1).matches("[a-zA-Z]")) {
                                                    latin_count++;
                                                    if (latin_count > 30) {
                                                        stemmedKeimeno = "";
                                                        latin_flag = true;
                                                        break;
                                                    }
                                                }
                                                stemmedKeimeno = stemmedKeimeno + word + "\t";
                                            }

                                            if (latin_flag) {
                                                for (String word : words) {
                                                    if (stopwords.contains(word) || word.length() <= 2) {
                                                        continue;
                                                    }
                                                    EnglishStemmer stemmer = new EnglishStemmer();
                                                    stemmer.add(word.toCharArray(), word.length());
                                                    stemmer.stem();
                                                    word = stemmer.toString();
                                                    stemmedKeimeno = stemmedKeimeno + word + "\t";
                                                }
                                            }

                                            try (
                                                    FileWriter fw = new FileWriter(System.getProperty("user.dir") + "/files/classification/level1/meka_training.arff", true);
                                                    BufferedWriter bw = new BufferedWriter(fw);
                                                    PrintWriter out = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File(System.getProperty("user.dir") + "/files/classification/level1/meka_training.arff"), true), "UTF-8"))) {
                                                //PrintWriter out = new PrintWriter(System.getProperty("user.dir") + "/files/classification/level1/meka_training.arff", "UTF-8")) {
                                                for (int i = 0; i < selections.length; i++) {
                                                    out.print(selections[i] + ",");
                                                }
                                                out.println("\'" + stemmedKeimeno + "\'" + "\t");
                                            } catch (IOException e) {
                                            }
                                        } catch (BadLocationException ex) {
                                            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                                        }
                                    }
                                });
                                cat.jButton2.addMouseListener(new MouseAdapter() {
                                    public void mouseClicked(MouseEvent evt) {
                                        cat.dispose();
                                        active = false;
                                    }
                                });
                                cat.addWindowListener(new WindowAdapter() {
                                    @Override
                                    public void windowClosing(WindowEvent we) {
                                        active = false;
                                    }
                                });

                            }
                        }
                    }
                    return null;
                }

                @Override

                protected void done() {
                }
            };
            worker.execute();
        }
    }

    class event11 extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent evt) {
            Point pt = evt.getPoint();
            if (evt.getClickCount() == 1) {
                int location = keimeno.jTextPane1.viewToModel(pt);
                StyledDocument doc = keimeno.jTextPane1.getStyledDocument();
                Element el = doc.getCharacterElement(location);
                AttributeSet set = el.getAttributes();
                if (set.containsAttribute("open", "just_do_it")) {
                    try {
                        String link = set.getAttribute("actual_link").toString();
                        link = link.replaceAll("\\s+", "");
                        java.awt.Desktop.getDesktop().browse(java.net.URI.create(link));
                    } catch (IOException ex) {
                        Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    class event12 extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent evt) {
            Point pt = evt.getPoint();
            if (evt.getClickCount() == 1) {
                int location = keimeno1.jTextPane1.viewToModel(pt);
                StyledDocument doc = keimeno1.jTextPane1.getStyledDocument();
                Element el = doc.getCharacterElement(location);
                AttributeSet set = el.getAttributes();
                if (set.containsAttribute("open", "just_do_it")) {
                    try {
                        String link = set.getAttribute("actual_link").toString();
                        link = link.replaceAll("\\s+", "");
                        java.awt.Desktop.getDesktop().browse(java.net.URI.create(link));
                    } catch (IOException ex) {
                        Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    class event3 implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JTextField addressText = new JTextField();
            addressText.setText(address);
            JTextField serverPortText = new JTextField();
            serverPortText.setText(serverPort);
            JTextField portText = new JTextField();
            portText.setText(port);
            JTextField usernameText = new JTextField();
            usernameText.setText(username);
            JPasswordField passwordText = new JPasswordField();
            passwordText.setText(password);
            JButton button = new JButton();
            button.setText(("Εκπαίδευση Μοντέλου"));
            final JComponent[] inputs = new JComponent[]{
                new JLabel("Network Address"),
                addressText, new JLabel("Server Port"),
                serverPortText,
                new JLabel("Database Port Number"),
                portText,
                new JLabel("Username"), usernameText,
                new JLabel("Password"),
                passwordText, button
            };
            button.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        System.out.println("Παρακαλώ περιμένετε...");
                        Article.exportModel();
                    } catch (Exception ex) {
                        Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            int result = JOptionPane.showConfirmDialog(null, inputs, "Ρυθμίσεις", JOptionPane.PLAIN_MESSAGE);
            if (result == JOptionPane.OK_OPTION) {

                GetPropertyValues prop = new GetPropertyValues();
                try {
                    address = addressText.getText();
                    serverPort = serverPortText.getText();
                    port = portText.getText();
                    username = usernameText.getText();
                    password = passwordText.getText();

                    prop.setPropValues();
                } catch (IOException ex) {
                    Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                System.out.println("User cancelled / closed the dialog, result = " + result);
            }
        }
    }

    class event1 implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            try {
                java.awt.Desktop.getDesktop().browse(java.net.URI.create("http://" + address + ":" + serverPort + "/keeon/application/add/add_from_check_view.php?key=" + sp.jLabel2.getText() + "&ef_source_id=&log_source_id=&allo_source_id=&keimeniko_eidos_id=&thematiki_enotita_id=&path_of_source=&date_of_source="));
            } catch (IOException ex) {
                Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    class event2 extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent evt) {
            Point pt = evt.getPoint();
            if (evt.getClickCount() == 2) {

                location = sp.jTextPane1.viewToModel(pt);

                StyledDocument doc = sp.jTextPane1.getStyledDocument();
                Element el = doc.getCharacterElement(location);
                AttributeSet set = el.getAttributes();
                RawWord.clear();

                if (set.containsAttribute(Foreground, Color.BLUE)) {
                    sp.setVisible(false);
                    value = (Integer) set.getAttribute("sentence");
                    wordIndex = (Integer) set.getAttribute("index");
                    String wordMatcher = wordsPanel.jTable1.getValueAt(wordIndex, 0).toString();
                    String result = "";
                    String test1 = "";
                    for (String test : original) {
                        test1 = test;
                        test = test.toUpperCase(new java.util.Locale("el"));
                        final Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");//$NON-NLS-1$
                        final String decomposed = Normalizer.normalize(test, Normalizer.Form.NFD);
                        test = pattern.matcher(decomposed).replaceAll("");//$NON-NLS-1$
                        if (test.equals(wordMatcher)) {
                            result = test1;
                            break;
                        }
                    }
                    keimeno.setSize(800, 800);
                    keimeno.jTextPane1.setText("");
                    keimeno.jTextField1.setText("");
                    keimeno.jTextPane2.setText("");
                    neologismos = new Word();
                    neologismos.wordMatcher(result);
                    add(keimeno);
                    keimeno.setVisible(true);

                    try {
                        wordCounter = 0;
                        keimeno.jTextField1.setText(keimena.get(value).getXml());
                        keimeno.jTextPane2.setText(keimena.get(value).getTitle());

                        doc = keimeno.jTextPane1.getStyledDocument();
                        SimpleAttributeSet keyWord = new SimpleAttributeSet();
                        StyleConstants.setForeground(keyWord, Color.BLUE);
                        StyleConstants.setBold(keyWord, true);

                        doc.insertString(doc.getLength(), keimena.get(value).getKeimeno(), null);

                        String document = doc.getText(doc.getStartPosition().getOffset(), doc.getEndPosition().getOffset() - doc.getStartPosition().getOffset());
                        String nonAccented = XmlReader.stripAccents(result);
                        String nonAccentedText = XmlReader.stripAccents(document);
                        int startposition = nonAccentedText.toLowerCase(new java.util.Locale("el")).indexOf(nonAccented.toLowerCase(new java.util.Locale("el")));

                        while (startposition >= 0) {
                            int endposition = startposition + result.length();
                            doc.remove(startposition, endposition - startposition);
                            doc.insertString(startposition, RawWord.get(wordCounter), keyWord);
                            startposition = nonAccentedText.toLowerCase(new java.util.Locale("el")).indexOf(nonAccented.toLowerCase(new java.util.Locale("el")), startposition + 1);
                            wordCounter++;
                        }
                        if (!Words.get(keimena.get(value).getArrayId()).isClassified()) {

                            SimpleAttributeSet question = new SimpleAttributeSet();
                            StyleConstants.setBold(question, true);

                            SimpleAttributeSet answer1 = new SimpleAttributeSet();
                            StyleConstants.setBold(answer1, true);
                            answer1.addAttribute("answer", "YES");
                            answer1.addAttribute("index", keimena.get(value).getArrayId());

                            SimpleAttributeSet answer2 = new SimpleAttributeSet();
                            StyleConstants.setBold(answer2, true);
                            answer2.addAttribute("answer", "NO");
                            answer2.addAttribute("index", keimena.get(value).getArrayId());

                            String classification = "Επιτυχής Κατηγοριοποίηση; (ΝΑΙ ΟΧΙ)";
                            String classification1 = "Επιτυχής Κατηγοριοποίηση; (";
                            String classification2 = ")";
                            int position = document.indexOf(classification);
                            int endposition = position + classification.length();
                            doc.remove(position, endposition - position);
                            doc.insertString(position, classification1, question);
                            int position1 = position + classification1.length();
                            doc.insertString(position1, "ΝΑΙ", answer1);
                            doc.insertString(position1 + 3, " ", null);
                            doc.insertString(position1 + 4, "ΟΧΙ", answer2);
                            doc.insertString(position1 + 7, ")", question);
                        }

                        SimpleAttributeSet link = new SimpleAttributeSet();
                        link.addAttribute("open", "just_do_it");

                        String linkInText = "Σύνδεσμος στο άρθρο: ";
                        int position = document.indexOf(linkInText);
                        int end = position + linkInText.length();

                        String nextMeta = "Ημερομ.";
                        int position1 = document.indexOf(nextMeta);

                        String le_link = doc.getText(end, position1 - end);
                        link.addAttribute("actual_link", le_link);
                        doc.remove(end, position1 - end);
                        doc.insertString(end, le_link, link);

                    } catch (BadLocationException ex) {
                        Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }

        }
    }

    class event4 implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            wordsPanel.setVisible(false);
            jScrollPane3.setVisible(true);
            jButton3.setVisible(true);
            jButton4.setVisible(true);
            jButton5.setVisible(true);
            jButton9.setVisible(true);
            jButton7.setVisible(true);
            jButton1.setVisible(true);
            jButton2.setVisible(true);
            jTextArea1.setVisible(true);
            jToolBar1.setVisible(true);
            jScrollPane3.setVisible(true);
            jScrollPane2.setVisible(true);
            jTable1.setVisible(true);
        }
    }

    class event5 implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            sp.setVisible(false);
            wordsPanel.setVisible(true);
            counter = 0;
            wordCounter = 0;
            RawWord.clear();
            Sentences.clear();
            keimena.clear();
            sp.jLabel2.setText("");
            sp.jTextPane1.setText("");
            value = 0;
            wordIndex = 0;
            sentenceIndex = 0;
            originalIndex = 0;
        }
    }

    class event6 implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            keimeno.setVisible(false);
            sp.setVisible(true);
            keimeno.jTextField1.setText("");
            keimeno.jTextPane2.setText("");
            keimeno.jTextPane1.setText("");
            value = 0;
            wordCounter = 0;
            wordIndex = 0;
            keimena.clear();
            RawWord.clear();
        }
    }

    class event9 implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            keimeno1.setVisible(false);
            keimeno1.jTextField1.setText("");
            keimeno1.jTextPane2.setText("");
            keimeno1.jTextPane1.setText("");
            jScrollPane3.setVisible(true);
            jButton3.setVisible(true);
            jButton4.setVisible(true);
            jButton5.setVisible(true);
            jButton9.setVisible(true);
            jButton7.setVisible(false);
            jButton1.setVisible(true);
            jButton2.setVisible(true);
            jTextArea1.setVisible(true);
            jToolBar1.setVisible(true);
            jScrollPane3.setVisible(true);
            jScrollPane2.setVisible(true);
            jTable1.setVisible(true);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    new MainFrame().setVisible(true);

                } catch (IOException ex) {
                    Logger.getLogger(MainFrame.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    public javax.swing.JButton jButton9;
    public javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JToolBar jToolBar1;
    // End of variables declaration//GEN-END:variables
}
